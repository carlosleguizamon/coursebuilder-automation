#!/usr/bin/env bash
#Publish xray results
json=$(curl -k 'https://xray.cloud.xpand-it.com/api/v1/authenticate' -X POST -H 'Content-Type: application/json' -d "{\"client_id\":\"$1\",\"client_secret\":\"$2\"}") && token=$(echo $json | sed "s/{.*\"token\":\"\([^\"]*\).*}/\1/g") && echo "token = $token"
token="$(echo "$token" | sed -e 's/^"//' -e 's/"$//')"
curl -X POST -H "Authorization: Bearer $token" -F info=@output/TestExecutionInfo.json -F results=@output/CucumberTestReport.json 'https://xray.cloud.xpand-it.com/api/v1/import/execution/cucumber/multipart'
echo "XRay publisher command executed"