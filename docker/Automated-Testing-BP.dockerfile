FROM dalmirog/base-java-jdk8:latest

WORKDIR /var/app/

COPY ./ ./

CMD ["sh", "-c", "mvn clean test exec:exec -Dcucumber.filter.tags='@TEST_EV-725'"]