ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST)))) #getting current execution dir

build_image: #Create the docker image
	docker build -t test/automated-testing-bp:latest -f docker/Automated-Testing-BP.dockerfile --no-cache .

remove_previous_config:
	docker stop selenium || echo true
	docker rm selenium || echo true
	docker network rm my-net || echo true

create_network:
	docker network create my-net

run_selenium: #Run the selenium server on network "my-net"
	docker run -d --network my-net --name selenium selenium/standalone-chrome

run_test: #Run the docker image on network "my-net"
	docker run --network my-net -v $(strip $(ROOT_DIR))/testreports:/var/app/testreports test/automated-testing-bp:latest

run: build_image remove_previous_config create_network run_selenium run_test