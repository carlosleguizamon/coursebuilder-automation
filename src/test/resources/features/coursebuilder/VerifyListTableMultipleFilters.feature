@TEST_EV-740
Feature: Verify multiple filters can be applied

  @Daily
  @UI_CourseBuilder_Daily
  Scenario: Verify multiple filters can be applied
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the List View button on Course Builder page
    When user inputs a text to several column filters on Course Builder page, the rows get filtered