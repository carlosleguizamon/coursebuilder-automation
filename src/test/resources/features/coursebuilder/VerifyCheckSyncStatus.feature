@TEST_EV-743
@TEST_EV-1176
Feature: Verify to be able to check sync status

  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to check sync status after syncronize
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user clicks the sync status button
    And user clicks the first line arrow in Previous Sync Jobs page
    Then the first folder name contains "<New folder name>" in Previous Sync Jobs page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Calendar-Based Framework (7 weeks) |