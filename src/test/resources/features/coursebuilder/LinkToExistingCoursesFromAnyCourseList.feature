@TEST_EV-700
Feature: Can link to existing courses from any course list.

  @TEST_EV-883
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test for EV-700 Can link to existing courses from any course list.
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Check that the "Go to Sub-Account" folder option takes to the canvas courses page
    And user clicks the Tree View button on Course Builder page
    And user clicks the options menu over folder "<Parent folder name>"
    And user clicks the Go to Sub-Account folder option on Course Builder page
    Then the Canvas Courses page opens in a new tab
    #Check that the "Go to Course" course option takes to the course page
    When user goes back to the Course Builder page
    And user clicks over the folder "<Parent folder name>" on Course Builder page
    And user clicks over the folder "<Children folder name>" on Course Builder page
    And user clicks the List View button on Course Builder page
    And user clicks the Go to Course link over the first displayed course on Course Builder page
    Then the Canvas Course page opens in a new tab

    Examples:
    | Parent folder name             | Children folder name |
    | Carlos's school of Engineering | QA Program           |