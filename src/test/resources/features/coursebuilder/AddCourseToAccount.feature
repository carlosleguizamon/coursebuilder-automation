@TEST_EV-726
Feature: Verify to be able to add courses to the account

  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to add courses to the account
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<First course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    Then the course "<Default course name>" is displayed on Course Builder

    Examples:
    | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | First course template              |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001      | Calendar-Based Framework (7 weeks) |