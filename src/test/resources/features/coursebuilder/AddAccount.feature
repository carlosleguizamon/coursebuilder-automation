@TEST_EV-725
Feature: Verify to be able to add new account

  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to add new account
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    Then the folder "<New folder name>" is displayed on Course Builder page

    Examples:
    | Parent folder name             | Default folder name | New folder name        |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] |