Feature: Verify courses can be duplicated

  Background: Log in and go to List View
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page

  @TEST_EV-1130
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test Case for EV-804 Select one or more courses to duplicate into existing or new term
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add courses
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename both courses
    And user clicks over the course name "<Default course name 1>" on Course Builder page
    And user renames the course to "<Course name 1>"
    And user clicks over the course name "<Default course name 2>" on Course Builder page
    And user renames the course to "<Course name 2>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check both courses
    And user checks the course "<Course name 1>" on Course Builder page
    And user checks the course "<Course name 2>" on Course Builder page
    #Verify duplicate courses window
    When user clicks the Duplicate Courses button on Course Builder page
    Then the course "<Course name 1>" is displayed on Duplicate Courses page
    And the course "<Course name 2>" is displayed on Duplicate Courses page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name 1 | Default course name 2 | Course name 1 | Course name 2 | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 2              | Course Name 001       | Course Name 002       | First Course  | Second Course | Calendar-Based Framework (7 weeks) |

  @TEST_EV-1171
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test Case for EV-851 Sync Course Duplication Changes to Canvas
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename course
    And user clicks over the course name "<Default course name>" on Course Builder page
    And user renames the course to "<New course name>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check course
    And user checks the course "<New course name>" on Course Builder page
    #Duplicate course
    When user clicks the Duplicate Courses button on Course Builder page
    And user selects 1 duplicate for course "<New course name>"
    And user clicks Add button for course "<New course name>"
    And user renames the duplication of "<New course name>" containing suffix "(1)" to "<Duplicated course name>" on Duplicate Courses page
    #Check that the Duplicate Courses window closes
    And user clicks Duplicate button on Duplicate Courses page
    Then the Sync All Changes button is displayed on Course Builder page
    #Check that the Syncing icon is displayed
    When user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    Then the Syncing icon is displayed for course "<Duplicated course name>" on Course Builder page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | New course name | Duplicated course name        | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | First Course    | Duplicated course [timestamp] | Calendar-Based Framework (7 weeks) |

  @TEST_EV-1131
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test Case for EV-852 Can delete a course from Duplicate Courses view
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename course
    And user clicks over the course name "<Default course name>" on Course Builder page
    And user renames the course to "<New course name>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check course
    And user checks the course "<New course name>" on Course Builder page
    #Duplicate course
    And user clicks the Duplicate Courses button on Course Builder page
    And user selects 1 duplicate for course "<New course name>"
    And user clicks Add button for course "<New course name>"
    And user renames the duplication of "<New course name>" containing suffix "(1)" to "<Duplicated course name>" on Duplicate Courses page
    #Check that the duplicated course can be deleted
    When user clicks Delete button for duplicated course "<Duplicated course name>" on Duplicate Courses page
    And user clicks accept in the popup window
    Then the duplicated course "<Duplicated course name>" is not displayed on Duplicate Courses page
    #Check that the host course can be deleted
    When user clicks Delete button for course "<New course name>" on Duplicate Courses page
    And user clicks accept in the popup window
    Then the course "<New course name>" is not displayed on Duplicate Courses page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | New course name | Duplicated course name        | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | First Course    | Duplicated course [timestamp] | Calendar-Based Framework (7 weeks) |

  @TEST_EV-1129
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test Case for EV-853 Can create any number of duplicates of a course
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Configure course
    And user clicks over the course name "<Default course name>" on Course Builder page
    And user renames the course to "<New course name>"
    And user clicks over the course code with course name "<New course name>" on Course Builder page
    And user edits the Course Code to "<New course code>" on Course Builder page
    And user clicks over the Tags cell with course name "<New course name>" on Course Builder page
    And user adds the tag "Test tag [timestamp]" on Course Builder page
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check course
    And user checks the course "<New course name>" on Course Builder page
    #Duplicate course
    And user clicks the Duplicate Courses button on Course Builder page
    And user selects "A,B,C" auto-suffix for course "<New course name>"
    And user selects 3 duplicate for course "<New course name>"
    And user clicks Add button for course "<New course name>"
    And user clicks Duplicate button on Duplicate Courses page
    #Check duplicated courses
    Then the duplicated course values of the 3 duplications match with the host course "<New course name>" having "A,B,C" auto-suffix

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | New course name | New course code        | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | First Course    | First Code [timestamp] | Calendar-Based Framework (7 weeks) |

  @TEST_EV-1128
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test Case for EV-8499 Set default fields for each course being duplicated
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Configure course
    And user clicks over the course name "<Default course name>" on Course Builder page
    And user renames the course to "<New course name>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check course
    And user checks the course "<New course name>" on Course Builder page
    #Duplicate course
    And user clicks the Duplicate Courses button on Course Builder page
    #Verify Destination Account dropdown
    When user clicks the Destination Account dropdown on Duplicate Courses page
    Then the Destination Account values are displayed on Duplicate Courses page
    #Verify Destination Term dropdown
    When user clicks the Destination Term dropdown on Duplicate Courses page
    Then the Destination Term values are displayed on Duplicate Courses page
    #Verify Start Date button
    When user clicks the Start Date button on Duplicate Courses page
    Then the date picker is displayed on Duplicate Courses page
    #Verify End Date button
    When user closes the date picker on Duplicate Courses page
    And user clicks the End Date button on Duplicate Courses page
    Then the date picker is displayed on Duplicate Courses page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | New course name | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | First Course    | Calendar-Based Framework (7 weeks) |

    @TEST_EV-1174
    @Daily
#    @UI_CourseBuilder_Daily TODO: uncomment once bug EV-1252 gets solved
    Scenario Outline: Test Case for EV-1094 Create a new term
      #Add account
      And user clicks the folder "<Parent folder name>"
      And user clicks the Tree View button on Course Builder page
      And user clicks Add Account button on Course Builder page
      And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
      #Add course
      And user clicks the folder "<New folder name>"
      And user clicks the List View button on Course Builder page
      And user clicks Add Course button on Course Builder page
      And user chooses to add "<Courses to add>" courses on Course Builder page
      And user selects the template "<Course template>" on Course Builder page
      And user clicks Add button on Course Builder page
      #Configure course
      And user clicks over the course name "<Default course name>" on Course Builder page
      And user renames the course to "<New course name>"
      #Sync changes
      And user clicks Sync All Changes button on Course Builder page
      And user clicks Accept button from toast to confirm the synchronization on Course Builder page
      And user waits for the courses to be loaded on Course Builder page
      #Check course
      And user checks the course "<New course name>" on Course Builder page
      #Duplicate course
      And user clicks the Duplicate Courses button on Course Builder page
      #With new term
      And user clicks the Destination Term dropdown on Duplicate Courses page
      And user clicks "Create New Term" dropdown option on Duplicate Courses page
      And user fills in the Create New Term form with "<Term name>", "<SIS ID>", "<Start day 1>", "<End day 1>"
      When user clicks Create button to create a new term on Duplicate Courses page
      Then the selected Destination Term dropdown value is "<Term name>" on Duplicate Courses page
      #And one duplication
      And user selects "1,2,3" auto-suffix for course "<New course name>"
      And user selects 1 duplicate for course "<New course name>"
      And user clicks Add button for course "<New course name>"
      And user renames the duplication of "<New course name>" containing suffix "(1)" to "<Duplicated course name>" on Duplicate Courses page
      And user clicks Duplicate button on Duplicate Courses page
      ###Course builder
      #Verify new term is displayed
      Then the Term column value of the duplicated course "<Duplicated course name>" is "<Term name>"
      #Edit new term
      When user clicks the Term column button for the duplicated course "<Duplicated course name>" on Course Builder page
      And user clicks the Edit New Term button on Course Builder page
      Then the Edit New Term modal Update button is displayed on Course Builder page
      When user fills in the Edit New Term form with "<Term name>", "<SIS ID>", "<Start day 2>", "<End day 2>"
      And user clicks Update button to update a new term on Duplicate Courses page
      And user clicks Update popup button on Course Builder page
      #Sync changes
      And user clicks Sync All Changes button on Course Builder page
      And user clicks Accept button from toast to confirm the synchronization on Course Builder page
      And user waits for the courses to be loaded on Course Builder page
      And user clicks the Go to Course link over the duplicated course "<Duplicated course name>" on Course Builder page
      ###Canvas
      When user clicks Settings link on the Canvas course page
      Then the "<Term name>", "<Start day 2>" and "<End day 2>" of the duplicated course "<Duplicated course name>" are displayed on the Canvas course settings page
      ###Terms page
      When user goes to page "https://everspring3.instructure.com/accounts/1/terms"
      Then the "<Term name>", "<Start day 2>" and "<End day 2>" are displayed on the Canvas terms page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | New course name | Duplicated course name        | Term name | SIS ID | Start day 1 | End day 1 | Start day 2 | End day 2 | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | First Course    | Duplicated course [timestamp] | Test      | sisid  | 5           | 25        | 10          | 20        | Calendar-Based Framework (7 weeks) |

  @TEST_EV-1167
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test for EV-1095 Create a new account
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Configure course
    And user clicks over the course name "<Default course name>" on Course Builder page
    And user renames the course to "<New course name>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check course
    And user checks the course "<New course name>" on Course Builder page
    #Duplicate course
    And user clicks the Duplicate Courses button on Course Builder page
    #With new account
    And user clicks the Destination Account dropdown on Duplicate Courses page
    And user clicks "Create New Account" dropdown option on Duplicate Courses page
    And user fills in the Create New Account form with name "<New folder name 2>" and parent account "<Parent folder name>" on Duplicate Courses page
    And user clicks the Create button for Create New Account form on Duplicate Courses page
    #And one duplication
    And user selects "1,2,3" auto-suffix for course "<New course name>"
    And user selects 1 duplicate for course "<New course name>"
    And user clicks Add button for course "<New course name>"
    And user renames the duplication of "<New course name>" containing suffix "(1)" to "<Duplicated course name>" on Duplicate Courses page
    When user clicks Duplicate button on Duplicate Courses page
    #Verify new account and duplication
    Then the folder "<New folder name 2>" is displayed on Course Builder page
    And user clicks the folder "<New folder name 2>"
    And the duplicated course "<Duplicated course name>" is displayed on Course Builder

    Examples:
      | Parent folder name             | Default folder name | New folder name        | New folder name 2       | Courses to add | Default course name | New course name | Duplicated course name        | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | QA Program B[timestamp] | 1              | Course Name 001     | First Course    | Duplicated course [timestamp] | Calendar-Based Framework (7 weeks) |