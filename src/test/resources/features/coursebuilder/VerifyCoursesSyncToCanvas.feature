@TEST_EV-741
Feature: Verify to be able to sync courses to the canvas

#  @Daily
#  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to sync courses to the canvas
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<First course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    And user clicks Sync All Changes button on Course Builder page
    Then the Check Sync Status button is displayed on Course Builder page
    When user clicks Close sync status window button on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<New folder name>", "<Parent folder name>"
#    Then the course "<Default course name>" is displayed under folder "<New folder name>", "<Parent folder name>" having a locked icon

    Examples:
    | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | First course template              |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | CourseName 001      | Calendar-Based Framework (7 weeks) |