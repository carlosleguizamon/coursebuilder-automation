Feature: Verify courses can be imported

  @TEST_EV-1127
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Test Case for EV-1082  Import course into one or more selected courses
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    #Add account
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    #Add course with 7-week framework
    And user clicks Add Course button on Course Builder page
    And user chooses to add "1" courses on Course Builder page
    And user selects the template "Calendar-Based Framework (7 weeks)" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename the course
    And user clicks over the course name "<Default course name 1>" on Course Builder page
    And user renames the course to "<Template course name>"
    #Add course without framework
    And user clicks Add Course button on Course Builder page
    And user chooses to add "1" courses on Course Builder page
    And user selects the template "Do not apply template" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename the course
    And user clicks over the course name "<Default course name 2>" on Course Builder page
    And user renames the course to "<No template course name>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Import course with framework to course without framework
    And user checks the course "<No template course name>" on Course Builder page
    And user clicks the Import Course button on Course Builder page
    And user fills the "Name" filter with "<Template course name>" on Import Content page
    And user checks the course "<Template course name>" on Import Content page
    When user clicks the Import button on Import Content page
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Verify the framework is applied to the course without framework
    Then the template "Calendar-Based Framework (7 weeks)" is applied to the course "<No template course name>"

    Examples:
    | Parent folder name             | Default folder name | New folder name        | Default course name 1 | Default course name 2 | No template course name | Template course name |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] | Course Name 001       | Course Name 002       | CourseWithoutTemplate   | CourseWithTemplate   |

  @TEST_EV-1254
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Using Course Import to re-run courses for a new term
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    #Add account
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    #Add course with 7-week framework
    And user clicks Add Course button on Course Builder page
    And user chooses to add "1" courses on Course Builder page
    And user selects the template "Calendar-Based Framework (7 weeks)" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename the course
    And user clicks over the course name "<Default course name 1>" on Course Builder page
    And user renames the course to "<Template course name>"
    #Add another course without framework
    And user clicks Add Course button on Course Builder page
    And user chooses to add "1" courses on Course Builder page
    And user selects the template "Do not apply template" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename the course
    And user clicks over the course name "<Default course name 2>" on Course Builder page
    And user renames the course to "<No template course name>"
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Import course with framework to course without framework
    And user checks the course "<No template course name>" on Course Builder page
    And user clicks the Import Course button on Course Builder page
    And user fills the "Name" filter with "<Template course name>" on Import Content page
    And user checks the course "<Template course name>" on Import Content page
    When user clicks the Import button on Import Content page
    #Sync changes
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Click "Go to Course" link
    And user clicks the Go to Course link over the course "<No template course name>" on Course Builder page
    ###Canvas
    When user clicks Settings link on the Canvas course page
    And user sets the start and end dates as "<Start day>" and "<End day>" on Canvas course settings page
    And user clicks Update Course Details button on Canvas course settings page
    Then the message "Course was successfully updated" is displayed on Canvas course settings page
    And the start and end dates for course "<No template course name>" are "<Start day>" and "<End day>" on Canvas course settings page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Default course name 1 | Default course name 2 | No template course name | Template course name | Start day | End day |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | Course Name 001       | Course Name 002       | CourseWithoutTemplate   | CourseWithTemplate   | 5         | 25      |