Feature: Verify bulk actions

  @TEST_EV-734
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to apply template in bulk
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    #Add first course
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<First course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    And user clicks over the course name "Course Name 001" on Course Builder page
    And user renames the course to "<Course 01>"
    #Add second course
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<First course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    And user clicks over the course name "Course Name 002" on Course Builder page
    And user renames the course to "<Course 02>"
    And user clicks the Select All Courses checkbox on Course Builder page
    And user clicks Apply framework button
    And user selects the template "<Second course template>" on Course Builder page
    And user clicks Apply button on Course Builder page
    And user clicks Apply framework button
    Then the Template button of the course "<Course 01>" contains the text "Calendar-Based Framework (7 weeks)"
    Then the Template button of the course "<Course 02>" contains the text "Calendar-Based Framework (7 weeks)"
    Examples:
      | Parent folder name             | Second course template             |Default folder name | New folder name        | Courses to add | First course template | Course 01        | Course 02        |
      | Carlos's school of Engineering | Calendar-Based Framework (7 weeks) |New Account         | QA Program [timestamp] | 1              | Do not apply template | Course Name 001  | Course Name 002  |

  @TEST_EV-733
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify bulk actions
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Mark the checkbox of the created course
    Then user selects the created course
    And the Apply framework button is displayed on Course Builder page
    And the Clear Selections button is displayed on Course Builder page
    And user clicks the radio buttons left aligned to the first course
    And the Course Settings button is displayed on Course Builder page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Calendar-Based Framework (7 weeks) |

  @TEST_EV-736
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to delete courses in bulk
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the List View button on Course Builder page
    #Add first course
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    And user clicks over the course name "Course Name 001" on Course Builder page
    And user renames the course to "<Course 01>"
    #Add second course
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    And user clicks over the course name "Course Name 002" on Course Builder page
    And user renames the course to "<Course 02>"
    #Delete courses
    And user clicks the Select All Courses checkbox on Course Builder page
    And user clicks the Delete (Bulk Actions) button on Course Builder page
    And user clicks accept in the popup window
    Then the course "<Course 01>" is not displayed on Course Builder page
    Then the course "<Course 02>" is not displayed on Course Builder page

    Examples:
      | Parent folder name             | Courses to add | Course template                    | Course 01  | Course 02  |
      | Carlos's school of Engineering | 1              | Calendar-Based Framework (7 weeks) | Course A01 | Course B02 |

  @TEST_EV-737
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to clear selections in bulk
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add courses
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Select all courses
    When user clicks the Select All Courses checkbox on Course Builder page
    Then all the courses are selected on Course Builder page
    #Clear selections
    When user clicks the Select All Courses checkbox on Course Builder page
    Then all the courses are unselected on Course Builder page

    Examples:
      | Parent folder name             | Default folder name | New folder name        | Courses to add | Course template                    |
      | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 5              | Calendar-Based Framework (7 weeks) |