@TEST_EV-728
Feature: Verify a course can be created without any template

  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify a course can be created without any template
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    And user selects the created course
    And user clicks Apply framework button
   Then the course "<Default course name>" does not contain any template

    Examples:
    | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | Course template       |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | Do not apply template |