@TEST_EV-731
Feature: Verify to be able to delete course and account

  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Verify to be able to delete course and account
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    #Add account
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    #Add course
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<Course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Delete course
    And user selects the created course
    And user clicks the Delete (Bulk Actions) button on Course Builder page
    And user clicks accept in the popup window
    Then the course "<Default course name>" is not displayed on Course Builder page
    #Delete account
    When user clicks the Tree View button on Course Builder page
    And user clicks the folder "<New folder name>"
    And user clicks the Delete folder option on Course Builder page
    And user clicks accept in the popup window
    Then the folder "<New folder name>" is not displayed on Course Builder page

    Examples:
    | Parent folder name             | Default folder name | New folder name        | Courses to add | Course template                    | Default course name |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Calendar-Based Framework (7 weeks) | Course Name 001      |