@TEST_AP-434
Feature: Apply course templates

  @TEST_EV-832
  @Daily
  @UI_CourseBuilder_Daily
  Scenario Outline: Apply course templates to new courses and existing courses
    #Login and create a course with a template
    Given user is logged on Everspring Canvas page
    When user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the folder "<Parent folder name>"
    And user clicks the Tree View button on Course Builder page
    And user clicks Add Account button on Course Builder page
    And user renames the folder "<Default folder name>" under folder "<Parent folder name>" to "<New folder name>"
    And user clicks the folder "<New folder name>"
    And user clicks the List View button on Course Builder page
    And user clicks Add Course button on Course Builder page
    And user chooses to add "<Courses to add>" courses on Course Builder page
    And user selects the template "<First course template>" on Course Builder page
    And user clicks Add button on Course Builder page
    #Rename the course
    And user clicks over the course name "<Default course name>" on Course Builder page
    And user renames the course to "<New course name>"
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    #And user clicks Close sync status window button on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check that the template was properly set
    Then the template "<First course template>" is applied to the course "<New course name>"
    #Apply other template to the course
    When user goes back to the Course Builder page
    And user clicks the folder "<New folder name>", "<Parent folder name>"
    And user selects the created course
    And user clicks Apply framework button
    And user selects the template "<Second course template>" on Course Builder page
    And user clicks Apply button on Course Builder page
    And user clicks Sync All Changes button on Course Builder page
    And user clicks Accept button from toast to confirm the synchronization on Course Builder page
    #And user clicks Close sync status window button on Course Builder page
    And user waits for the courses to be loaded on Course Builder page
    #Check that the template was properly applied
    Then the template "<Second course template>" is applied to the course "<New course name>"

    Examples:
    | Parent folder name             | Default folder name | New folder name        | Courses to add | Default course name | New course name       | First course template              | Second course template              |
    | Carlos's school of Engineering | New Account         | QA Program [timestamp] | 1              | Course Name 001     | QA Course [timestamp] | Calendar-Based Framework (7 weeks) | Calendar-Based Framework (14 weeks) |