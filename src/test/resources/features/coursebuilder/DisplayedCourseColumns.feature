Feature: Verify displayed course column filters works

  Background: Log in and go to List View
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the List View button on Course Builder page

  @TEST_EV-738
  @Daily
  @UI_CourseBuilder_Daily
  Scenario: Verify to be able to edit the coulmns list in list view
    When user clicks the Columns to Display dropdown on Course Builder page
    And user checks the column "Name" for Columns to Display on Course Builder page
    Then the column "Name" is not displayed on Course Builder page
    When user checks the column "Name" for Columns to Display on Course Builder page
    Then the column "Name" is displayed on Course Builder page

  @TEST_EV-1047
  @Daily
  @UI_CourseBuilder_Daily
  Scenario: Test for EV-993 I can continue to click on column headers to turn them off
    When user clicks the Columns to Display dropdown on Course Builder page
    And user checks the column "Name" for Columns to Display on Course Builder page
    And user checks the column "Course Code" for Columns to Display on Course Builder page
    Then the Columns to Display dropdown is opened on Course Builder page
    When user clicks the Columns to Display dropdown on Course Builder page
    Then the Columns to Display dropdown is collapsed on Course Builder page
    When user clicks the Columns to Display dropdown on Course Builder page
    And user presses the "ESCAPE" button
    Then the Columns to Display dropdown is collapsed on Course Builder page

  @TEST_EV-722
  @Daily
  @UI_CourseBuilder_Daily
  Scenario: Test for EV-426 Select the headers I want to show in the courses list table
    When user clicks the Columns to Display dropdown on Course Builder page
    And user unchecks every column on Columns to Display dropdown on Course Builder page
    Then no columns are displayed on Course Builder page
    When user checks every column on Columns to Display dropdown on Course Builder page
    Then all columns are displayed on Course Builder page