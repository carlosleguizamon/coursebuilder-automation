@TEST_EV-875
Feature: Verify can drag account structure pane to resize

  @Daily
  @UI_CourseBuilder_Daily
  Scenario: Verify can drag account structure pane to resize
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    And user clicks the drag resize button
    Then sub-account tree browser should be hidden
