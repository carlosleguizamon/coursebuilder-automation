@TEST_EV-705
Feature: Verify the tree view and list table after initial login

  @TEST_EV-819
  @Daily
  @UI_CourseBuilder_Daily
  Scenario: Verify the tree view after initial login
    Given user is logged on Everspring Canvas page
    And user goes to Canvas Courses page for the configured account
    And user clicks Course Builder button on Everspring Courses page
    And user waits for the courses to be loaded on Course Builder page
    Then the tree view button is displayed on Course Builder page