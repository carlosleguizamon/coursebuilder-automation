package steps;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features"},
        plugin = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:output/CucumberTestReport.json",
                "junit:testreports/Cucumber.xml",
                "rerun:target/cucumber-reports/rerun.txt"
        }
)
public class RunCucumberTest {

        @BeforeClass
        public static void setup() {
        }

}