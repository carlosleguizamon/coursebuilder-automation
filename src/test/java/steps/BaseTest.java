package steps;


import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.cucumber.java.Scenario;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.*;

import java.util.ArrayList;

public class BaseTest {

    protected static final Config CONFIG = ConfigFactory.load("configuration.conf");

    protected static WebDriver driver;
    private static Report report;

    public static void beforeSuite() throws Exception {
        //Start driver
        String testMode = CONFIG.getString("testMode");
        driver = WebDriverFactory.createWebDriver(testMode);
        //Get reporter
        report = ExtentReport.getInstance();
    }

    public static void tearDown(Scenario scenario) {
        report.addMethodResult(scenario.getName(), scenario.getStatus());
        if(scenario.isFailed()){
            driver.switchTo().defaultContent();
            Screenshot screenshot = new AShot()
                    .shootingStrategy(ShootingStrategies.viewportPasting(100))
                    .takeScreenshot(driver);
            report.appendScreenshot(scenario.getName(), screenshot);
        }
        report.closeReport();
    }

    protected void closeDriver() {
        try{
            JsonUtils.deleteWebDriverSessionConfigFile();
            closeDriver(driver);
        } catch (NoSuchSessionException e){
            //The driver was already closed, do nothing
        }
    }

    protected void closeDriver(WebDriver driver) {
        driver.close();
        driver.quit();
    }

    protected void restartDriver() throws Exception {
        closeDriver();
        beforeSuite();
    }

    protected void openNewBrowserTab(){
        ((JavascriptExecutor)driver).executeScript("window.open()");
    }

    protected void switchBrowserTab(){
        String currentTab = driver.getWindowHandle();
        for(String tab: driver.getWindowHandles()){
            if(!tab.equals(currentTab)){
                driver.switchTo().window(tab);
                break;
            }
        }
    }

    protected void closeBrowserTab(){
        driver.close();
        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0).toString());
    }

    protected String getCurrentUrlWithoutParameters(){
        String currentUrl = driver.getCurrentUrl();
        if(!currentUrl.contains("?")){
            return currentUrl;
        }
        else{
            return currentUrl.split("\\?")[0];
        }
    }

}
