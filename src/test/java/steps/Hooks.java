package steps;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utils.ExtentReport;
import utils.XrayExporter;

public class Hooks extends BaseTest {

    @Before
    public void before(Scenario scenario) throws Exception {
        XrayExporter.createTestExecutionInfo();
        BaseTest.beforeSuite();
        ExtentReport.createTest(scenario.getName());
    }

    @After
    public void afterScenario(Scenario scenario){
        BaseTest.tearDown(scenario);
    }

}