package steps;

import io.cucumber.java.en.And;
import org.openqa.selenium.Keys;

public class GlobalStepdefs extends BaseTest {

    @And("user goes to page {string}")
    public void userGoesToPage(String page) {
        driver.get(page);
    }

    @And("user presses the {string} button")
    public void userPressesTheButton(String button) {
        driver.switchTo().activeElement().sendKeys(Keys.valueOf(button));
    }
}
