package steps.coursebuilder;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageobjects.canvas.DuplicateCoursesComponent;
import pageobjects.canvas.NewTermComponent;
import steps.BaseTest;
import utils.DynamicCourseDataHandler;

public class DuplicateCoursesStepDefs extends BaseTest {
    private DynamicCourseDataHandler dynamicCourseDataHandler = DynamicCourseDataHandler.getInstance();
    private DuplicateCoursesComponent duplicateCoursesComponent;
    private NewTermComponent newTermComponent;

    @Then("the course {string} is displayed on Duplicate Courses page")
    public void theCourseIsDisplayedOnDuplicateCoursesPage(String courseName) {
        duplicateCoursesComponent = new DuplicateCoursesComponent(driver);
        Assert.assertTrue(String.format("The course '%s' was not displayed on Duplicate Courses component", courseName), duplicateCoursesComponent.isCourseDisplayed(courseName));
    }

    @And("user selects {int} duplicate for course {string}")
    public void userSelectsDuplicateForCourse(int duplicates, String courseName) {
        duplicateCoursesComponent = new DuplicateCoursesComponent(driver);
        duplicateCoursesComponent.selectDuplicatesForCourse(duplicates, courseName);
    }

    @And("user selects {string} auto-suffix for course {string}")
    public void userSelectsAutoSuffixForCourse(String autoSuffix, String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        duplicateCoursesComponent = new DuplicateCoursesComponent(driver);
        duplicateCoursesComponent.selectAutoSuffixForCourse(autoSuffix, courseName);
    }

    @And("user clicks Add button for course {string}")
    public void userClicksAddButtonForCourse(String courseName) {
        duplicateCoursesComponent.clickAddButtonForCourse(courseName);
    }

    @And("user clicks Duplicate button on Duplicate Courses page")
    public void userClicksDuplicateButtonOnDuplicateCoursesPage() {
        duplicateCoursesComponent.clickDuplicateButton();
    }

    @And("user renames the duplication of {string} containing suffix {string} to {string} on Duplicate Courses page")
    public void userRenamesTheDuplicationOfContainingSuffixToOnDuplicateCoursesPage(String duplicatedCourseName, String suffix, String duplicatedNewName) {
        dynamicCourseDataHandler.setRandomDuplicatedCourseName(duplicatedNewName);
        duplicatedNewName = dynamicCourseDataHandler.getDuplicatedCourseName();
        duplicateCoursesComponent.renameCourseDuplicate(duplicatedCourseName, suffix, duplicatedNewName);
    }

    @When("user clicks Delete button for duplicated course {string} on Duplicate Courses page")
    public void userClicksDeleteButtonForDuplicatedCourseOnDuplicateCoursesPage(String duplicatedName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            duplicatedName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        duplicateCoursesComponent.clickDeleteButtonForCourseDuplicate(duplicatedName);
    }

    @Then("the duplicated course {string} is not displayed on Duplicate Courses page")
    public void theDuplicatedCourseIsNotDisplayedOnDuplicateCoursesPage(String duplicatedName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            duplicatedName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        Assert.assertFalse(String.format("The duplicated course '%s' was displayed on Duplicate Courses page", duplicatedName), duplicateCoursesComponent.getDuplicatedNames().contains(duplicatedName));
    }

    @When("user clicks Delete button for course {string} on Duplicate Courses page")
    public void userClicksDeleteButtonForCourseOnDuplicateCoursesPage(String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        duplicateCoursesComponent.clickDeleteButtonForCourseHost(courseName);
    }

    @Then("the course {string} is not displayed on Duplicate Courses page")
    public void theCourseIsNotDisplayedOnDuplicateCoursesPage(String courseName) {
        Assert.assertFalse(String.format("The course '%s' was displayed on Duplicate Courses component", courseName), duplicateCoursesComponent.isCourseDisplayed(courseName));
    }

    @When("user clicks the Destination Account dropdown on Duplicate Courses page")
    public void userClicksTheDestinationAccountDropdownOnDuplicateCoursesPage() {
        duplicateCoursesComponent = new DuplicateCoursesComponent(driver);
        duplicateCoursesComponent.clickDestinationAccountDropdown();
    }

    @Then("the Destination Account values are displayed on Duplicate Courses page")
    public void theDestinationAccountValuesAreDisplayedOnDuplicateCoursesPage() {
        Assert.assertTrue("The Destination Account dropdown did not contain values on Duplicate Courses page",
                duplicateCoursesComponent.getCurrentDropdownValues().size() > 0);
    }

    @When("user clicks the Destination Term dropdown on Duplicate Courses page")
    public void userClicksTheDestinationTermDropdownOnDuplicateCoursesPage() {
        duplicateCoursesComponent = new DuplicateCoursesComponent(driver);
        duplicateCoursesComponent.clickDestinationTermDropdown();
    }

    @Then("the Destination Term values are displayed on Duplicate Courses page")
    public void theDestinationTermValuesAreDisplayedOnDuplicateCoursesPage() {
        Assert.assertTrue("The Destination Term dropdown did not contain values on Duplicate Courses page",
                duplicateCoursesComponent.getCurrentDropdownValues().size() > 0);
    }

    @When("user clicks the Start Date button on Duplicate Courses page")
    public void userClicksTheStartDateButtonOnDuplicateCoursesPage() {
        newTermComponent = new NewTermComponent(driver);
        newTermComponent.clickStartDateButton();
    }

    @And("user clicks the End Date button on Duplicate Courses page")
    public void userClicksTheEndDateButtonOnDuplicateCoursesPage() {
        newTermComponent.clickEndDateButton();
    }

    @Then("the date picker is displayed on Duplicate Courses page")
    public void theDatePickerIsDisplayedOnDuplicateCoursesPage() {
        Assert.assertTrue("The date picker was not displayed on Duplicate Courses page", newTermComponent.isDatePickerDisplayed());
    }

    @When("user closes the date picker on Duplicate Courses page")
    public void userClosesTheDatePickerOnDuplicateCoursesPage() {
        newTermComponent.clickDatePickerCloseButton();
    }

    @And("user clicks {string} dropdown option on Duplicate Courses page")
    public void userClicksDropdownOptionOnDuplicateCoursesPage(String dropdownOption) {
        duplicateCoursesComponent.clickCurrentDropdownValue(dropdownOption);
    }

    @And("user fills in the Create New Term form with {string}, {string}, {string}, {string}")
    public void userFillsInTheCreateNewTermFormWith(String termName, String sisId, String startDayOfMonth, String endDayOfMonth) {
        newTermComponent = new NewTermComponent(driver);
        termName = dynamicCourseDataHandler.setRandomTermName(termName);
        sisId = dynamicCourseDataHandler.setRandomSisId(sisId);
        newTermComponent.fillInNewTermForm(termName, sisId, startDayOfMonth, endDayOfMonth);
    }

    @When("user clicks Create button to create a new term on Duplicate Courses page")
    public void userClicksCreateButtonToCreateANewTermOnDuplicateCoursesPage() {
        newTermComponent.clickSubmitButton();
    }

    @Then("the selected Destination Term dropdown value is {string} on Duplicate Courses page")
    public void theSelectedDestinationTermDropdownValueIsOnDuplicateCoursesPage(String termName) {
        if(dynamicCourseDataHandler.getTermName() != null){
            termName = dynamicCourseDataHandler.getTermName();
        }
        Assert.assertEquals(String.format("The term '%s' was not selected for dropdown Destination Term on Duplicate Courses page", termName), duplicateCoursesComponent.getSelectedDestinationTerm(), termName);
    }

    @And("user fills in the Create New Account form with name {string} and parent account {string} on Duplicate Courses page")
    public void userFillsInTheCreateNewAccountFormWithNameAndParentAccount(String newAccountName, String parentFolderName) {
        newAccountName = dynamicCourseDataHandler.setRandomChildrenFolderName(newAccountName);
        duplicateCoursesComponent.clickDestinationAccountDropdown();
        duplicateCoursesComponent.clickCurrentDropdownValue(parentFolderName);
        duplicateCoursesComponent.fillInCreateNewAccountForm(newAccountName, parentFolderName);
    }

    @And("user clicks the Create button for Create New Account form on Duplicate Courses page")
    public void userClicksTheCreateButtonForCreateNewAccountFormOnDuplicateCoursesPage() {
        duplicateCoursesComponent.clickCreateNewAccountFormCreateButton();
    }
}
