package steps.coursebuilder;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageobjects.canvas.*;
import steps.BaseTest;
import utils.CourseUtils;
import utils.DateUtils;
import utils.DynamicCourseDataHandler;
import utils.ExtentReport;

import java.util.List;

public class CBGlobalStepDefs extends BaseTest {
    private DynamicCourseDataHandler dynamicCourseDataHandler = DynamicCourseDataHandler.getInstance();
    private CanvasLoginPage canvasLoginPage;
    CanvasMenuComponent canvasMenuComponent;
    private CanvasCourseBuilderComponent canvasCourseBuilderComponent;
    private NewTermComponent newTermComponent;
    private ImportContentComponent importContentComponent;
    private CanvasSettingsPage canvasSettingsPage;

    @Given("user is logged on Everspring Canvas page")
    public void userIsLoggedOnEverspringCanvasPage() throws Exception {
        restartDriver();
        final String email = CONFIG.getString("everspringCourseBuilderEmail");
        final String password = CONFIG.getString("everspringCourseBuilderPassword");
        canvasLoginPage = new CanvasLoginPage(driver);
        canvasLoginPage.goToPage();
        canvasLoginPage.isPage();
        canvasLoginPage.login(email, password);
    }

    @When("user goes to Canvas Courses page for the configured account")
    public void userGoesToCanvasCoursesPageForAccount() {
        CanvasCoursesPage canvasCoursesPage = new CanvasCoursesPage(driver);
        canvasCoursesPage.goToPage();
        Assert.assertTrue("The button Course (to create a new course) was not displayed", canvasCoursesPage.isPage());
    }

    @Then("the Canvas Courses page opens in a new tab")
    public void theCanvasCoursesPageOpensInANewTab() {
        //Check that the page that opens is the expected one
        switchBrowserTab();
        CanvasCoursesPage canvasCoursesPage = new CanvasCoursesPage(driver);
        Assert.assertTrue("The button Course (to create a new course) was not displayed", canvasCoursesPage.isPage());
    }

    @Then("the Canvas Course page opens in a new tab")
    public void theCanvasCoursePageOpensInANewTab() {
        //Check that the page that opens is the expected one
        switchBrowserTab();
        TemplateBasePage templateBasePage = new TemplateBasePage(driver);
        Assert.assertTrue("The icon Import Existing Content on Canvas Course page was not displayed", templateBasePage.isPage());
    }

    @When("user goes back to the Course Builder page")
    public void userGoesBackToTheCourseBuilderPage() {
        //Close the new tab
        closeBrowserTab();
        canvasCourseBuilderComponent = new CanvasCourseBuilderComponent(driver);
    }

    @And("user clicks Course Builder icon on Everspring Courses page")
    public void userClicksCourseBuilderIconOnEverspringCoursesPage() {
        CanvasLeftHeaderComponent canvasLeftHeaderComponent = new CanvasLeftHeaderComponent(driver);
        canvasLeftHeaderComponent.clickCourseBuilder();
    }

    @And("user clicks Course Builder button on Everspring Courses page")
    public void userClicksCourseBuilderButtonOnEverspringCoursesPage() {
        canvasMenuComponent = new CanvasMenuComponent(driver);
        canvasMenuComponent.clickCourseBuilderSection();
    }

    @When("user clicks Settings link on the Canvas course page")
    public void userClicksSettingsLinkOnTheCanvasCoursePage() {
        switchBrowserTab();
        canvasMenuComponent.clickSettingsSection();
    }

    @And("user waits for the courses to be loaded on Course Builder page")
    public void userWaitsForTheCoursesToBeLoadedOnCourseBuilderPage() {
        canvasCourseBuilderComponent = new CanvasCourseBuilderComponent(driver);
        canvasCourseBuilderComponent.clickAuthorizeButtonIfDisplayed();
        canvasCourseBuilderComponent.waitForCoursesToBeLoaded();
    }

    @Then("the tree view button is displayed on Course Builder page")
    public void theTreeStructureIsDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("The tree courses structure was not displayed on Course Builder page",
                canvasCourseBuilderComponent.isTreeViewButtonDisplayed());
    }

    @When("user clicks over the folder {string} on Course Builder page")
    public void userClicksOverTheFolderOnCourseBuilderPage(String folderName) {
        canvasCourseBuilderComponent.clickFolder(folderName);
    }

    @When("user clicks Add Account button on Course Builder page")
    public void userClicksAddFolderButtonOverTheFolder() {
        canvasCourseBuilderComponent.clickAddAccountButton();
    }

    @And("user clicks Add Course button on Course Builder page")
    public void userClicksAddCourseButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickAddCoursesButton();
    }

    @And("user renames the folder {string} under folder {string} to {string}")
    public void userRenamesTheFolderUnderFolderTo(String defaultFolderName, String parentFolderName, String newChildrenFolderName) {
        dynamicCourseDataHandler.setRandomChildrenFolderName(newChildrenFolderName);
        canvasCourseBuilderComponent.renameChildrenFolder(defaultFolderName, parentFolderName, dynamicCourseDataHandler.getChildrenFolderName());
    }

    @And("user chooses to add {string} courses on Course Builder page")
    public void userChoosesToAddCoursesOnCourseBuilderPage(String coursesToAdd) {
        canvasCourseBuilderComponent.fillInCoursesToAdd(coursesToAdd);
    }

    @And("user selects the template {string} on Course Builder page")
    public void userSelectsTheTemplateOnCourseBuilderPage(String courseTemplate) {
        canvasCourseBuilderComponent.selectCourseTemplate(courseTemplate);
    }

    @And("user clicks Add button on Course Builder page")
    public void userClicksAddButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickAdd();
    }

    @Then("the course {string} is displayed on Course Builder")
    public void theCourseIsDisplayedUnderFolder(String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        Assert.assertTrue("The course was not displayed on Course Builder page",
                canvasCourseBuilderComponent.isCourseDisplayed(courseName));
    }

    @Then("the course {string} is not displayed on Course Builder page")
    public void theCourseIsNotDisplayedUnderFolder(String courseName) {
        Assert.assertFalse("The course was displayed on Course Builder page, but should not be displayed",
                canvasCourseBuilderComponent.isCourseDisplayed(courseName));
    }

    @Then("the duplicated course {string} is displayed on Course Builder")
    public void theDuplicatedCourseIsDisplayedUnderFolder(String duplicatedCourseName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            duplicatedCourseName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        Assert.assertTrue("The duplicated course was not displayed on Course Builder page",
                canvasCourseBuilderComponent.isCourseDisplayed(duplicatedCourseName));
    }

    @And("user checks the course {string} on Course Builder page")
    public void userChecksTheCourseOnCourseBuilderPage(String courseName) {
        canvasCourseBuilderComponent.clickCourseCheckbox(courseName);
    }

    @When("user clicks the options menu over folder {string}")
    public void userClicksTheOptionsMenuOverFolder(String folderName) {
        canvasCourseBuilderComponent.clickMenuButtonOverFolder(folderName);
    }

    @When("user clicks the options menu over folder {string}, {string}")
    public void userClicksTheOptionsMenuOverFolder(String childrenFolderName, String parentFolderName) {
        childrenFolderName = dynamicCourseDataHandler.getChildrenFolderName();
        canvasCourseBuilderComponent.clickMenuButtonOverChildrenFolder(childrenFolderName, parentFolderName);
    }

    @And("user clicks the Delete folder option on Course Builder page")
    public void userClicksTheDeleteFolderOptionOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickDeleteAccount();
    }

    @Then("the Apply framework button is displayed on Course Builder page")
    public void theApplyFrameworkButtonIsDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("The 'Apply Template' button was not displayed", canvasCourseBuilderComponent.isApplyFrameworkButtonDisplayed());
    }

    @Then("the Course Settings button is displayed on Course Builder page")
    public void theCourseSettingsButtonIsDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("The 'Course Settings' button was not displayed", canvasCourseBuilderComponent.isCourseSettingsButtonDisplayed());
    }

    @Then("the Clear Selections button is displayed on Course Builder page")
    public void theClearSelectionsButtonIsDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("The 'Clear Selections' button was not displayed", canvasCourseBuilderComponent.isClearSelectionsButtonDisplayed());
    }

    @And("user clicks the Delete \\(Bulk Actions) button on Course Builder page")
    public void userClicksTheDeleteBulkActionsButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickBulkActionsDeleteButton();
    }

    @And("user clicks the Go to Sub-Account folder option on Course Builder page")
    public void userClicksTheGoToSubAccountFolderOptionOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickGoToSubAccountFolderOption();
    }

    @Then("the folder {string} is displayed on Course Builder page")
    public void theFolderIsDisplayedOnCourseBuilderPage(String folderName) {
        if(dynamicCourseDataHandler.getChildrenFolderName() != null){
            folderName = dynamicCourseDataHandler.getChildrenFolderName();
        }
        Assert.assertTrue(String.format("The folder '%s' was not displayed on Course Builder page", folderName),
                canvasCourseBuilderComponent.isFolderDisplayed(folderName));
    }

    @Then("the folder {string} is not displayed on Course Builder page")
    public void theFolderIsNotDisplayedOnCourseBuilderPage(String folderName) {
        if(dynamicCourseDataHandler.getChildrenFolderName() != null){
            folderName = dynamicCourseDataHandler.getChildrenFolderName();
        }
        Assert.assertFalse(String.format("The folder '%s' was not deleted on Course Builder page", folderName),
                canvasCourseBuilderComponent.isFolderDisplayed(folderName));
    }

    @And("user clicks the List View button on Course Builder page")
    public void userClicksTheListViewButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickListViewButton();
    }

    @And("user clicks the Tree View button on Course Builder page")
    public void userClicksTheTreeViewButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickTreeViewButton();
    }

    @When("user inputs a text at each column filter on Course Builder page, the rows get filtered")
    public void userAppliesAFilterToEachColumnHeaderOnCourseBuilderPage() {
        applyCourseFilter(true);
    }

    @When("user inputs a text to several column filters on Course Builder page, the rows get filtered")
    public void userAppliesSeveralFiltersToColumnHeadersOnCourseBuilderPage() {
        applyCourseFilter(false);
    }

    private void applyCourseFilter(boolean clearFilterAfterEachStep){
        List<Integer> courseFilterIndexes = canvasCourseBuilderComponent.getCourseColumnIndexes();
        //For each filter
        for(Integer filterColumnIndex: courseFilterIndexes){
            ExtentReport.log("Testing filter with index " + filterColumnIndex);
            //Get content of the first row under the current column
            String firstRowCellText = canvasCourseBuilderComponent.getCell(0, filterColumnIndex);
            ExtentReport.log("First row cell text is " + firstRowCellText);
            //Apply a filter using the first word
            String firstWord = firstRowCellText.split(" ")[0];
            canvasCourseBuilderComponent.applyCourseFilter(filterColumnIndex, firstWord);
            //Get content of all rows under the current column
            List<String> cellsUnderColumn = canvasCourseBuilderComponent.getAllCellsUnderColumn(filterColumnIndex);
            //Check that each cell matches the filter
            for(String cell: cellsUnderColumn){
                Assert.assertTrue("A cell that does not match the filter criteria was found",
                        cell.toLowerCase().contains(firstWord.toLowerCase()));
            }
            //Clear filter
            ExtentReport.log("Filter worked properly");
            if(clearFilterAfterEachStep){
                canvasCourseBuilderComponent.clearCourseFilter(filterColumnIndex);
            }
        }
    }

    @Then("the Check Sync Status button is displayed on Course Builder page")
    public void theCheckSyncStatusButtonIsDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("The button 'Check Sync Status' was not displayed on Course Builder page",
                canvasCourseBuilderComponent.isCheckSyncStatusButtonDisplayed());
    }

    @And("user clicks Sync All Changes button on Course Builder page")
    public void userClicksSyncChangesButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickSyncAllChangesButton();
    }

    @And("user renames the course to {string}")
    public void userRenamesTheCourseUnderFolderTo(String newCourseName) {
        dynamicCourseDataHandler.setRandomCourseName(newCourseName);
        newCourseName = dynamicCourseDataHandler.getCourseName();
        canvasCourseBuilderComponent.renameCourse(newCourseName);
    }

    @And("user edits the Course Code to {string} on Course Builder page")
    public void userEditsTheCourseCodeToOnCourseBuilderPage(String newCourseCode) {
        newCourseCode = dynamicCourseDataHandler.setRandomCourseCode(newCourseCode);
        canvasCourseBuilderComponent.renameCourseCode(newCourseCode);
    }

    @And("user adds the tag {string} on Course Builder page")
    public void userAddsTheTagOnCourseBuilderPage(String newTag) {
        newTag = dynamicCourseDataHandler.setRandomTag(newTag);
        canvasCourseBuilderComponent.addCourseTag(newTag);
    }

    @And("user clicks Close sync status window button on Course Builder page")
    public void userClicksCloseSyncStatusWindowButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickCloseSyncStatusWindow();
    }

    @And("user clicks the folder {string}")
    public void userClicksTheFolder(String folderName) {
        if(folderName.contains(DynamicCourseDataHandler.TIMESTAMP)){
            folderName = dynamicCourseDataHandler.getChildrenFolderName();
        }
        canvasCourseBuilderComponent.clickFolder(folderName);
    }

    @And("user clicks the folder {string}, {string}")
    public void userClicksTheFolder(String childrenFolderName, String parentFolderName) {
        childrenFolderName = dynamicCourseDataHandler.getChildrenFolderName();
        canvasCourseBuilderComponent.clickSavedChildrenFolder(childrenFolderName, parentFolderName);
    }

    @And("user clicks the Template button of the saved course {string}")
    public void userClicksTheTemplateButtonOfTheCourseUnder(String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        canvasCourseBuilderComponent.clickSavedCourseTemplateButton(courseName);
    }

    @Then("the Template button of the course {string} contains the text {string}")
    public void theTemplateButtonOfTheCourseContainsTheText(String courseName, String templateName) {
        Assert.assertEquals(String.format("The Template for the course %s does not contain the expected text", courseName),
                templateName, canvasCourseBuilderComponent.getSelectedTemplateText());
    }

    @And("user clicks the options menu over the course {string} on Course Builder page")
    public void userClicksTheOptionsMenuOverCourseUnder(String courseName) {
        canvasCourseBuilderComponent.clickMenuButtonOverCourse(courseName);
    }

    @And("user clicks the options menu over the first displayed course under {string}, {string}")
    public void userClicksTheOptionsMenuOverTheFirstDisplayedCourseUnder(String childrenFolderName, String parentFolderName) {
        canvasCourseBuilderComponent.clickMenuButtonOverFirstDisplayedCourse(childrenFolderName, parentFolderName);
    }

    @And("user clicks the Go to Course link over the first displayed course on Course Builder page")
    public void userClicksTheGoToCourseCourseLinkOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickGoToCourseLinkOverFirstDisplayedCourse();
    }

    @And("user clicks Apply button on Course Builder page")
    public void userClicksApplyButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickApply();
    }

    @Then("the template {string} is applied to the course {string}")
    public void theTemplateIsAppliedToTheCourse(String templateName, String courseName) {
        //Open the course page in a new tab
        openNewBrowserTab();
        switchBrowserTab();
        CanvasCoursesPage canvasCoursesPage = new CanvasCoursesPage(driver);
        canvasCoursesPage.goToPage();
        Assert.assertTrue("The button Course (to create a new course) was not displayed", canvasCoursesPage.isPage());

        //Search the course and click it
        courseName = dynamicCourseDataHandler.getCourseName();
        canvasCoursesPage.search(courseName);
        canvasCoursesPage.clickCourseLink(courseName);

        //Check that the template is the expected one
        String errorMessage = "The page template for course '%s' does not match, should be '%s'";
        switch(templateName){
            case "Calendar-Based Framework (7 weeks)":
                CalendarBasedFramework7WeeksTemplatePage cBF7WTemplate = new CalendarBasedFramework7WeeksTemplatePage(driver);
                Assert.assertTrue(String.format(errorMessage, courseName, "Calendar-Based Framework (7 weeks)"), cBF7WTemplate.isPage());
                break;
            case "Calendar-Based Framework (14 weeks)":
                CalendarBasedFramework14WeeksTemplatePage cBF14WTemplate = new CalendarBasedFramework14WeeksTemplatePage(driver);
                Assert.assertTrue(String.format(errorMessage, courseName, "Calendar-Based Framework (14 weeks)"), cBF14WTemplate.isPage());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + templateName);
        }
    }

    @Then("the course list filters are displayed")
    public void theCourseListHeadersAreDisplayed() {
        Assert.assertTrue(canvasCourseBuilderComponent.getCourseColumnIndexes().size() > 0);
    }

    @And("user clicks accept in the popup window")
    public void userClicksAcceptInThePopupWindow() {
        canvasCourseBuilderComponent.clickAcceptPopupButton();

    }

    @Then("user clicks the sync status button")
    public void userclicksCheckSyncStatus() {
        canvasCourseBuilderComponent.clickSyncButton();
    }

    @And("user clicks Accept button from toast to confirm the synchronization on Course Builder page")
    public void userClicksAcceptButtonFromToastToConfirmTheSynchronizationOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickAcceptToast();
    }

    @And("user clicks the Select All Courses checkbox on Course Builder page")
    public void userClicksTheSelectAllCoursesCheckboxOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickSelectAllCourses();
    }

    @Then("all the courses are selected on Course Builder page")
    public void allTheCoursesAreSelectedOnCourseBuilderPage() {
        Assert.assertTrue("Not all of the courses were selected on Course Builder page", canvasCourseBuilderComponent.areAllCoursesSelected());
    }

    @Then("all the courses are unselected on Course Builder page")
    public void allTheCoursesAreUnselectedOnCourseBuilderPage() {
        Assert.assertTrue("Not all of the courses were unselected on Course Builder page", canvasCourseBuilderComponent.areAllCoursesUnselected());
    }

    @And("user clicks Apply framework button")
    public void userClicksApplyFrameworkTButton() {
        canvasCourseBuilderComponent.clickApplyFramework();
    }


    @And("user clicks the first line arrow in Previous Sync Jobs page")
    public void userClicksTheFirstLineArrowInPreviousSyncJobsPage() throws InterruptedException {
        canvasCourseBuilderComponent.clickButtonShowSyncJobFirstRow();
    }


    @Then("the first folder name contains {string} in Previous Sync Jobs page")
    public void userChecksTheFolderIsDisplayed(String childrenFolderName) {
        childrenFolderName= dynamicCourseDataHandler.getChildrenFolderName();
        Assert.assertEquals("The folder name does not match with the expected text",
                childrenFolderName ,canvasCourseBuilderComponent.getFirstPreviousSyncJobFolderName());
    }

    @And("user selects the created course")
    public void userSelectsTheCreatedCourse() {
        canvasCourseBuilderComponent.clickSelectCourse();
    }

    @Then("the course {string} does not contain any template")
    public void theTemplateButtonOfTheCourseDoesNotContainAnyTemplate(String courseName) {
        Assert.assertTrue(String.format("the course %s did contain a template selected, but should have none", courseName), canvasCourseBuilderComponent.noFrameWorkIsSelected());
    }

    @And("user clicks the radio buttons left aligned to the first course")
    public void userClicksTheRadioButtonsLeftAlignedToTheCourse() {
        canvasCourseBuilderComponent.clickFirstCourseRadioButtons();
    }

    @And("user clicks the drag resize button")
    public void userClicksTheDragResizeButton() {
        canvasCourseBuilderComponent.clickResizeAccount();
    }

    @Then("sub-account tree browser should be hidden")
    public void subAccountTreeBrowserShouldBeHidden() {
        Assert.assertTrue("the account column is visible to user", canvasCourseBuilderComponent.accountSectionIsHidden());
    }

    @And("user clicks over the course name {string} on Course Builder page")
    public void userClicksOverTheCourseNameOnCourseBuilderPage(String courseName) {
        canvasCourseBuilderComponent.clickCourseNameButton(courseName);
    }

    @And("user clicks over the course code with course name {string} on Course Builder page")
    public void userClicksOverTheCourseCodeOnCourseBuilderPage(String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        canvasCourseBuilderComponent.clickCourseCodeButton(courseName);
    }

    @And("user clicks over the Tags cell with course name {string} on Course Builder page")
    public void userClicksOverTheTagsCellWithCourseNameOnCourseBuilderPage(String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        canvasCourseBuilderComponent.clickCourseTagButton(courseName);
    }

    @When("user clicks the Columns to Display dropdown on Course Builder page")
    public void userClicksTheColumnsToDisplayDropdownOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickColumnsToDisplayDropdown();
    }

    @And("user checks the column {string} for Columns to Display on Course Builder page")
    public void userChecksTheColumnForColumnsToDisplayOnCourseBuilderPage(String columnName) {
        canvasCourseBuilderComponent.checkColumnToDisplay(columnName);
    }

    @Then("the column {string} is not displayed on Course Builder page")
    public void theColumnIsNotDisplayedOnCourseBuilderPage(String columnName) {
        Assert.assertFalse(String.format("The column '%s' was displayed on Course Builder page", columnName), canvasCourseBuilderComponent.isColumnDisplayed(columnName));
    }

    @Then("the column {string} is displayed on Course Builder page")
    public void theColumnIsDisplayedOnCourseBuilderPage(String columnName) {
        Assert.assertTrue(String.format("The column '%s' was not displayed on Course Builder page", columnName), canvasCourseBuilderComponent.isColumnDisplayed(columnName));
    }

    @Then("the Columns to Display dropdown is opened on Course Builder page")
    public void theColumnsToDisplayDropdownIsOpenedOnCourseBuilderPage() {
        Assert.assertTrue("The Columns to Display dropdown was not opened on Course Builder page", canvasCourseBuilderComponent.isColumnsToDisplayDropdownOpened());
    }

    @Then("the Columns to Display dropdown is collapsed on Course Builder page")
    public void theColumnsToDisplayDropdownIsCollapsedOnCourseBuilderPage() {
        Assert.assertFalse("The Columns to Display dropdown was not collapsed on Course Builder page", canvasCourseBuilderComponent.isColumnsToDisplayDropdownOpened());
    }

    @And("user checks every column on Columns to Display dropdown on Course Builder page")
    public void userChecksEveryColumnOnColumnsToDisplayDropdownOnCourseBuilderPage() {
        canvasCourseBuilderComponent.checkAllColumnsToDisplay();
    }

    @And("user unchecks every column on Columns to Display dropdown on Course Builder page")
    public void userUnchecksEveryColumnOnColumnsToDisplayDropdownOnCourseBuilderPage() {
        canvasCourseBuilderComponent.uncheckAllColumnsToDisplay();
    }

    @Then("no columns are displayed on Course Builder page")
    public void noColumnsAreDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("Some columns were displayed on Course Builder page", canvasCourseBuilderComponent.getCourseColumnNames().isEmpty());
    }

    @Then("all columns are displayed on Course Builder page")
    public void allColumnsAreDisplayedOnCourseBuilderPage() {
        List<String> columnsDisplayed = canvasCourseBuilderComponent.getCourseColumnNames();
        List<String> allColumnsFromDropdown = canvasCourseBuilderComponent.getColumnsToDisplayDropdownNames();
        Assert.assertEquals("Some columns were not displayed on Course Builder page", allColumnsFromDropdown, columnsDisplayed);
    }

    @When("user clicks the Duplicate Courses button on Course Builder page")
    public void userClicksTheDuplicateCoursesButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickDuplicateCoursesButton();
    }

    @When("user clicks the Import Course button on Course Builder page")
    public void userClicksTheImportCourseButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickImportCourseButton();
    }

    @Then("the Sync All Changes button is displayed on Course Builder page")
    public void theSyncAllChangesButtonIsDisplayedOnCourseBuilderPage() {
        Assert.assertTrue("The Sync All Changes button was not displayed on Course Builder page", canvasCourseBuilderComponent.isSyncAllChangesButtonDisplayed());
    }

    @Then("the Syncing icon is displayed for course {string} on Course Builder page")
    public void theSpinningSyncIconIsDisplayedForCourseOnCourseBuilderPage(String courseName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            courseName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        Assert.assertTrue(String.format("The Syncing icon was not displayed for course '%s'", courseName), canvasCourseBuilderComponent.isSyncingIconDisplayedForCourse(courseName));
    }

    @Then("the duplicated course values of the {int} duplications match with the host course {string} having {string} auto-suffix")
    public void theDuplicatedCourseValuesOfTheDuplicationsMatchWithTheHostCourseHavingAutoSuffix(int numberOfDuplicates, String hostCourseName, String autoSuffixType) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            hostCourseName = dynamicCourseDataHandler.getCourseName();
        }
        //Verify host course is displayed
        Assert.assertTrue(String.format("The host course '%s' was not displayed on Course Builder page", hostCourseName), canvasCourseBuilderComponent.isCourseDisplayed(hostCourseName));

        List<String> hostTags = canvasCourseBuilderComponent.getCourseTags(hostCourseName);
        //For each duplicate
        for(int i=1; i<=numberOfDuplicates; i++){
            //Verify course duplicate is displayed and name matches suffix
            String duplicateSuffix = CourseUtils.getCourseDuplicateSuffix(autoSuffixType, i);
            String duplicateName = String.format("%s (%s)", hostCourseName, duplicateSuffix);
            Assert.assertTrue(String.format("The duplicate course '%s' was not displayed on Course Builder page", duplicateName), canvasCourseBuilderComponent.isCourseDisplayed(duplicateName));
            //Verify course duplicate contains the tags of the host course
            List<String> duplicateTags = canvasCourseBuilderComponent.getCourseTags(duplicateName);
            Assert.assertEquals(String.format("The Tags of the course duplicate '%s' do not match with the Tags of the host course", duplicateName), hostTags, duplicateTags);
            //Verify course code matches suffix
            String duplicateCodeExpected = String.format("%s (%s)", dynamicCourseDataHandler.getCourseCode(), duplicateSuffix);
            Assert.assertEquals(String.format("The course code of the course duplicate '%s' does not match with the course code of the host course", duplicateName),
                    duplicateCodeExpected, canvasCourseBuilderComponent.getCourseCode(duplicateName));
        }
    }

    @Then("the Term column value of the duplicated course {string} is {string}")
    public void theTermColumnValueOfTheCourseIs(String courseName, String termName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            courseName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        if(dynamicCourseDataHandler.getTermName() != null){
            termName = dynamicCourseDataHandler.getTermName();
        }
        Assert.assertEquals(String.format("The Term column value for the duplicated course '%s' does not match", courseName), termName, canvasCourseBuilderComponent.getCourseTermName(courseName));
    }

    @When("user clicks the Term column button for the duplicated course {string} on Course Builder page")
    public void userClicksTheTermColumnButtonForTheDuplicatedCourseOnCourseBuilderPage(String courseName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            courseName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        canvasCourseBuilderComponent.clickCourseTermButton(courseName);
    }

    @And("user clicks the Edit New Term button on Course Builder page")
    public void userClicksTheEditNewTermButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickEditNewTermButton();
    }

    @Then("the Edit New Term modal Update button is displayed on Course Builder page")
    public void theEditNewTermModalIsDisplayedOnCourseBuilderPage() {
        newTermComponent = new NewTermComponent(driver);
        Assert.assertTrue("The Edit New Term modal Update button was not displayed on Course Builder page", newTermComponent.isSubmitButtonDisplayed());
    }

    @When("user fills in the Edit New Term form with {string}, {string}, {string}, {string}")
    public void userFillsInTheEditNewTermFormWith(String termName, String sisId, String startDayOfMonth, String endDayOfMonth) {
        termName = dynamicCourseDataHandler.setRandomTermName(termName);
        sisId = dynamicCourseDataHandler.setRandomSisId(sisId);
        newTermComponent.fillInNewTermForm(termName, sisId, startDayOfMonth, endDayOfMonth);
    }

    @And("user clicks Update button to update a new term on Duplicate Courses page")
    public void userClicksUpdateButtonToUpdateANewTermOnDuplicateCoursesPage() {
        newTermComponent.clickSubmitButton();
    }

    @And("user clicks the Go to Course link over the duplicated course {string} on Course Builder page")
    public void userClicksTheGoToCourseLinkOverTheDuplicatedCourseOnCourseBuilderPage(String duplicatedCourseName) {
        if(dynamicCourseDataHandler.getDuplicatedCourseName() != null){
            duplicatedCourseName = dynamicCourseDataHandler.getDuplicatedCourseName();
        }
        canvasCourseBuilderComponent.clickGoToCourse(duplicatedCourseName);
    }

    @And("user clicks the Go to Course link over the course {string} on Course Builder page")
    public void userClicksTheGoToCourseLinkOverTheCourseOnCourseBuilderPage(String courseName) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        canvasCourseBuilderComponent.clickGoToCourse(courseName);
    }

    @Then("the {string}, {string} and {string} of the duplicated course {string} are displayed on the Canvas course settings page")
    public void theAndAreDisplayedOnTheCanvasCourseSettingsPage(String termName, String startDayOfMonth, String endDayOfMonth, String duplicatedCourseName) {
        duplicatedCourseName = dynamicCourseDataHandler.getDuplicatedCourseName();
        if(dynamicCourseDataHandler.getTermName() != null){
            termName = dynamicCourseDataHandler.getTermName();
        }
        String startDate = DateUtils.getCurrentMonthDateByDay(CanvasSettingsPage.DATE_FORMAT_DAY_ONLY, startDayOfMonth);
        String endDate = DateUtils.getCurrentMonthDateByDay(CanvasSettingsPage.DATE_FORMAT_DAY_ONLY, endDayOfMonth);

        canvasSettingsPage = new CanvasSettingsPage(driver);

        Assert.assertEquals(String.format("Canvas Settings page Term name does not match for course '%s'", duplicatedCourseName), termName, canvasSettingsPage.getTermName());
        Assert.assertEquals(String.format("Canvas Settings page Start date does not match for course '%s'", duplicatedCourseName), startDate, canvasSettingsPage.getStartDate());
        Assert.assertEquals(String.format("Canvas Settings page End date does not match for course '%s'", duplicatedCourseName), endDate, canvasSettingsPage.getEndDate());
    }

    @And("user clicks Update popup button on Course Builder page")
    public void userClicksUpdatePopupButtonOnCourseBuilderPage() {
        canvasCourseBuilderComponent.clickPopupUpdateButton();
    }

    @Then("the {string}, {string} and {string} are displayed on the Canvas terms page")
    public void theAndAreDisplayedOnTheCanvasTermsPage(String termName, String startDayOfMonth, String endDayOfMonth) {
        if(dynamicCourseDataHandler.getTermName() != null){
            termName = dynamicCourseDataHandler.getTermName();
        }
        String startDate = DateUtils.getCurrentMonthDateByDay(CanvasTermsPage.DATE_FORMAT, startDayOfMonth);
        String endDate = DateUtils.getCurrentMonthDateByDay(CanvasTermsPage.DATE_FORMAT, endDayOfMonth);

        CanvasTermsPage canvasTermsPage = new CanvasTermsPage(driver);

        Assert.assertTrue(String.format("The term '%s' was not found on Course Builder Terms page", termName), canvasTermsPage.isTermDisplayedByName(termName));
        Assert.assertEquals(String.format("The Start date of the term '%s' does not match on Course Builder Terms page", termName), startDate, canvasTermsPage.getStartDate(termName));
        Assert.assertEquals(String.format("The End date of the term '%s' does not match on Course Builder Terms page", termName), endDate, canvasTermsPage.getEndDate(termName));
    }

    @And("user fills the {string} filter with {string} on Import Content page")
    public void userFillsTheFilterWithOnImportContentPage(String columnName, String textFilter) {
        importContentComponent = new ImportContentComponent(driver);
        importContentComponent.filter(columnName, textFilter);
    }

    @And("user checks the course {string} on Import Content page")
    public void userChecksTheCourseOnImportContentPage(String courseName) {
        importContentComponent.checkCourseByName(courseName);
    }

    @When("user clicks the Import button on Import Content page")
    public void userClicksTheImportButtonOnImportContentPage() {
        importContentComponent.clickImportButton();
    }

    @Then("the message {string} is displayed on Canvas course settings page")
    public void theMessageIsDisplayedOnCanvasCourseSettingsPage(String message) {
        Assert.assertTrue(String.format("The message '%s' was not displayed on Canvas course settings page", message), canvasSettingsPage.isMessageDisplayed(message));
    }

    @And("user sets the start and end dates as {string} and {string} on Canvas course settings page")
    public void userSetsTheStartAndEndDatesAsAndOnCanvasCourseSettingsPage(String startDayOfMonth, String endDayOfMonth) {
        canvasSettingsPage = new CanvasSettingsPage(driver);
        canvasSettingsPage.setStartDateForCurrentMonth(startDayOfMonth);
        canvasSettingsPage.setEndDateForCurrentMonth(endDayOfMonth);
    }

    @And("user clicks Update Course Details button on Canvas course settings page")
    public void userClicksUpdateCourseDetailsButtonOnCanvasCourseSettingsPage() {
        canvasSettingsPage.clickUpdateCourseDetailsButton();
    }

    @And("the start and end dates for course {string} are {string} and {string} on Canvas course settings page")
    public void theStartAndEndDatesAreAndOnCanvasCourseSettingsPage(String courseName, String startDayOfMonth, String endDayOfMonth) {
        if(dynamicCourseDataHandler.getCourseName() != null){
            courseName = dynamicCourseDataHandler.getCourseName();
        }
        String startDate = DateUtils.getCurrentMonthDateByDay(CanvasSettingsPage.DATE_FORMAT_DAY_ONLY, startDayOfMonth);
        String endDate = DateUtils.getCurrentMonthDateByDay(CanvasSettingsPage.DATE_FORMAT_DAY_ONLY, endDayOfMonth);

        canvasSettingsPage = new CanvasSettingsPage(driver);

        Assert.assertEquals(String.format("Canvas Settings page Start date does not match for course '%s'", courseName), startDate, canvasSettingsPage.getStartDate());
        Assert.assertEquals(String.format("Canvas Settings page End date does not match for course '%s'", courseName), endDate, canvasSettingsPage.getEndDate());
    }
}
