package utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class JsonUtils {

    private static final String ROOT_PATH = "";
    private static final String BROWSER_CONFIG_PATH = "src/test/resources/browsers/";
    private static final String CONFIGS_PATH = "src/test/resources/configs/";
    private static final Config CONFIG = ConfigFactory.load("configuration.conf");
    private static final String WEBDRIVER_SESSION_CONFIG_FILENAME = "currentDriverSession.json";

    public static String findJsonToString(String path, String fileName) {
        return findJsonToString(path.concat(fileName));
    }

    public static String findJsonToString(String fullFilePath) {
        try {
            return FileUtils.readFileToString(new File(fullFilePath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static JsonObject getFileAsJsonObject(String path, String fileName) throws FileNotFoundException {
        try{
            return new Gson().fromJson(findJsonToString(path, fileName), JsonObject.class);
        } catch (RuntimeException e){
            String errorMessage = String.format("Could not find the file '%s'", path.concat(fileName));
            ExtentReport.log(errorMessage);
            throw new FileNotFoundException(errorMessage);
        }
    }

    public static JsonObject getFileAsJsonObject(String fullFilePath) {
        return new Gson().fromJson(findJsonToString(fullFilePath), JsonObject.class);
    }

    public static void deleteFile(String path, String fileName){
        FileUtils.deleteQuietly(new File(path.concat(fileName)));
    }

    public static HashMap findBrowserJsonToHashMap() {
        String fileName = "";
        String browser = CONFIG.getString("browser");
        switch (browser){
            case "safari":
                fileName = "mac";
                break;
            case "chrome":
                fileName = "windows";
                break;
            default:
                System.out.println("Error: browser configuration not recognized");
        }
        return new Gson().fromJson(findJsonToString(BROWSER_CONFIG_PATH, fileName.concat(".json")), HashMap.class);
    }

    public static String findAssigneeIdByName(String name){
        HashMap assignees = new Gson().fromJson(findJsonToString(CONFIGS_PATH, "xray_assignees.json"), HashMap.class);
        return (String) assignees.get(name);
    }

    public static void createWebDriverSessionConfigFile(String url, String sessionId){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("url", url);
        jsonObject.addProperty("sessionId", sessionId);
        try {
            FileWriter fileWriter = new FileWriter(ROOT_PATH + WEBDRIVER_SESSION_CONFIG_FILENAME);
            fileWriter.write(jsonObject.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            ExtentReport.log("An exception occurred when trying to create the file " + WEBDRIVER_SESSION_CONFIG_FILENAME);
            e.printStackTrace();
        }
    }

    /**
     * Updates the specified file by replacing the "data" json attribute with the data received as parameter.
     * The json "updated_at" attribute is also modified with the current date and time.
     * @param destinationPath
     * @param fileName
     * @param data
     * @throws FileNotFoundException
     */
    public static void updateJsonFileWithData(String destinationPath, String fileName, JsonElement data) throws FileNotFoundException {
        updateJsonFileWithData(destinationPath + fileName, data);
    }

    public static void updateJsonFileWithData(String filePath, JsonElement data) throws FileNotFoundException {
        //Get the file content
        File file = new File(filePath);
        JsonObject currentFile;
        if(file.isFile()) {
            currentFile = new Gson().fromJson(findJsonToString(filePath), JsonObject.class);
        }
        else{
            //If the file does not exist, throw an exception
            String error = String.format("The file '%s' does not exist. The update operation cannot proceed", filePath);
            ExtentReport.log(error);
            throw new FileNotFoundException(error);
        }

        //Add the data to the json file
        JsonObject fileToSave = new JsonObject();
        fileToSave.add("data", data);

        //Add date information
        if(currentFile != null && currentFile.has("created_at")){
            fileToSave.addProperty("created_at", currentFile.get("created_at").getAsString());
        }
        else{
            fileToSave.addProperty("created_at", DateUtils.getCurrentTime());
        }
        fileToSave.addProperty("updated_at", DateUtils.getCurrentTime());

        //Save the file
        saveFile(filePath, fileToSave);
    }

    /**
     * Adds the attributes as a json array under the "data" attribute. If the file already exists, appends said data to the "data" attribute.
     * If the file does not exist, first creates the file and then adds the attributes as a json array under the "data" attribute.
     * @param destinationPath
     * @param fileName
     * @param attributes
     */
    public static void createOrAppendJsonFileWithDataArray(String destinationPath, String fileName, Map<String, String> attributes){
        JsonArray data = new JsonArray();
        File file = new File(destinationPath + fileName);
        //If the file exists, get the content under "data" attribute
        JsonObject currentFile = null;
        if(file.isFile()) {
            currentFile = new Gson().fromJson(findJsonToString(destinationPath, fileName), JsonObject.class);
            //Get current data as array
            currentFile.getAsJsonArray("data").forEach(
                    element -> data.add(element)
            );
        }

        //Parse the new content
        JsonObject newContent = new JsonObject();
        for(String key: attributes.keySet()){
            newContent.addProperty(key, attributes.get(key));
        }

        //Append new content with previous content
        data.add(newContent);

        //Add data to new json file
        JsonObject fileToSave = new JsonObject();
        fileToSave.add("data", data);

        //Add date information
        if(currentFile != null && currentFile.has("created_at")){
            fileToSave.addProperty("created_at", currentFile.get("created_at").getAsString());
        }
        else{
            fileToSave.addProperty("created_at", DateUtils.getCurrentTime());
        }
        fileToSave.addProperty("updated_at", DateUtils.getCurrentTime());

        //Save the file
        saveFile(destinationPath, fileName, fileToSave);
    }

    public static void createJsonFileWithEmptyData(String destinationPath){
        //Add empty data array
        JsonObject fileToCreate = new JsonObject();
        fileToCreate.add("data", new JsonArray());

        //Add date information
        fileToCreate.addProperty("created_at", DateUtils.getCurrentTime());
        fileToCreate.addProperty("updated_at", DateUtils.getCurrentTime());

        //Save the file
        saveFile(destinationPath, fileToCreate);
    }

    public static void createJsonFileWithEmptyData(String destinationPath, String fileName){
        createJsonFileWithEmptyData(destinationPath.concat(fileName));
    }

    private static void saveFile(String destinationPath, String fileName, JsonObject jsonFileToSave){
        saveFile(destinationPath + fileName,jsonFileToSave);
    }

    private static void saveFile(String destinationPath, JsonObject jsonFileToSave){
        try {
            FileWriter fileWriter = new FileWriter(destinationPath);
            fileWriter.write(jsonFileToSave.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            ExtentReport.log(String.format("An exception occurred when trying to write the file '%s'", destinationPath));
            e.printStackTrace();
        }
    }

    public static HashMap getWebDriverSessionConfigFile(){
        try{
            return new Gson().fromJson(findJsonToString(ROOT_PATH, WEBDRIVER_SESSION_CONFIG_FILENAME), HashMap.class);
        } catch (RuntimeException ex){
            return null;
        }
    }

    public static void deleteWebDriverSessionConfigFile(){
        deleteFile(ROOT_PATH, WEBDRIVER_SESSION_CONFIG_FILENAME);
    }

}
