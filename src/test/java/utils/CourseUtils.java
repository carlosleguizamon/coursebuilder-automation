package utils;

public class CourseUtils {

    private static final String DUPLICATE_COURSE_AUTO_SUFFIX_ABC = "A,B,C";
    private static final String DUPLICATE_COURSE_AUTO_SUFFIX_123 = "1,2,3";

    public static String getCourseDuplicateSuffix(String autoSuffixType, int numberOfDuplicate){
        switch (autoSuffixType){
            case DUPLICATE_COURSE_AUTO_SUFFIX_ABC:
                return String.valueOf((char) ('A' + numberOfDuplicate - 1));
            case DUPLICATE_COURSE_AUTO_SUFFIX_123:
                return String.valueOf(numberOfDuplicate);
        }
        throw new IllegalArgumentException(String.format("Unsupported course auto-suffix type: '%s'", autoSuffixType));
    }
}
