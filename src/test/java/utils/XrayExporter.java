package utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class XrayExporter {

    private static final Config CONFIG = ConfigFactory.load("configuration.conf");

    private static final String UI_COURSE_BUILDER = "UI CourseBuilder"; //@UI_CourseBuilder_Daily

    public static void createTestExecutionInfo() throws IOException {
        //Create test execution json info
        LocalDateTime today = LocalDateTime.now();
        String todayStr = today.getYear() + "-" + today.getMonth() + "-" + today.getDayOfMonth() + " " + today.getHour() + ":" + today.getMinute() + ":" + today.getSecond() + ":" + today.getNano();
        String testExecutionName = getTestExecutionName();
        String testExecutionNameAndHour = testExecutionName + " " + todayStr;
        String assigneeId = JsonUtils.findAssigneeIdByName(CONFIG.getString("xrayTicketAssignee"));
        String jsonString = "{\n" +
                "  \"fields\": {\n" +
                "    \"project\": {\n" +
                "      \"key\": \"WD\"\n" +
                "    },\n" +
                "    \"summary\": \"" + testExecutionNameAndHour + "\",\n" +
                "    \"issuetype\": {\n" +
                "      \"id\": \"11333\"\n" +
                "    },\n" +
                "    \"assignee\": {\n" +
                "       \"id\": \"" + assigneeId + "\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        //Export to file
        File file = new File("output/TestExecutionInfo.json");
        file.createNewFile();
        try (PrintWriter out = new PrintWriter("output/TestExecutionInfo.json")) {
            out.println(jsonString);
        }
    }

    private static String getTestExecutionName(){
        return UI_COURSE_BUILDER;
    }

}
