package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleCommandExecutor {

    private static final String COMMAND_EXTRACT_IP_FROM_DOMAIN = "getent ahostsv4 %s | awk '{print $1}' | head -1";

    public static String resolveDomainToIp(String dns) throws IOException, InterruptedException {
        String output = runBashCommand(String.format(COMMAND_EXTRACT_IP_FROM_DOMAIN, dns));
        return output.replaceAll("/n", "").trim();
    }

    private static String runBashCommand(String command) throws IOException, InterruptedException {
        //Execute command
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);
        Process process = processBuilder.start();
        process.waitFor();

        //Get output
        BufferedReader buffer = new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder output = new StringBuilder();
        String line;
        while ((line = buffer.readLine()) != null) {
            output.append(line).append("\n");
        }

        return output.toString();
    }

}
