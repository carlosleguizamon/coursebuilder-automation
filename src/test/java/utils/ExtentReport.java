package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.cucumber.java.Status;
import ru.yandex.qatools.ashot.Screenshot;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class ExtentReport implements Report {
    private final String REPORT_PATH = "./output/";
    private final String SCREENSHOTS_PATH = "./output/screenshots/";
    private ExtentHtmlReporter htmlReporter;
    private static ExtentReports extent;
    private static ExtentTest test;
    private String scenarioName;

    static final Config CONFIG = ConfigFactory.load("configuration.conf");
    private static ExtentReport INSTANCE = null;

    private synchronized static void createInstance() throws IOException {
        if (INSTANCE == null) {
            INSTANCE = new ExtentReport();
        }
    }

    public static ExtentReport getInstance() throws IOException {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    private ExtentReport() throws IOException {
        //Create report directory if doesn't exist yet
        File htmlFolder = new File(REPORT_PATH);
        if(!htmlFolder.exists()){
            htmlFolder.mkdirs();
        }
        //Create report file
        File htmlFile = new File(REPORT_PATH.concat("extent-report.html"));
        htmlFile.createNewFile();
        htmlReporter = new ExtentHtmlReporter(htmlFile);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        //Add environment info
        String browser = CONFIG.getString("browser");
        extent.setSystemInfo("Browser", browser);
        //Layout report variables
        htmlReporter.config().setDocumentTitle("Everspring");
        htmlReporter.config().setReportName("Everspring QA Test Report");
    }

    //Created before each test
    public static void createTest(String scenarioName){
        test = extent.createTest(scenarioName);
    }

    public void addMethodResult(String scenarioName, Status result){
        //Write to report
        if(result == Status.FAILED) {
            test.log(com.aventstack.extentreports.Status.FAIL, MarkupHelper.createLabel(scenarioName+" FAILED ", ExtentColor.RED));
        }
        else if(result == Status.PASSED) {
            test.log(com.aventstack.extentreports.Status.PASS, MarkupHelper.createLabel(scenarioName+" PASSED ", ExtentColor.GREEN));
        }
        else {
            test.log(com.aventstack.extentreports.Status.SKIP, MarkupHelper.createLabel(scenarioName+" SKIPPED ", ExtentColor.ORANGE));
        }
    }

    public void log(String title, String content){
        test.log(com.aventstack.extentreports.Status.INFO, MarkupHelper.createLabel(title + ":", ExtentColor.GREEN));
        test.log(com.aventstack.extentreports.Status.INFO, content);
    }

    public static void log(String content){
        test.log(com.aventstack.extentreports.Status.INFO, content);
        System.out.println(content); //To show the output during jenkins execution also
    }

    public void appendScreenshot(String scenarioName, Screenshot screenshot){
        //Create directory if doesn't exist yet
        File screenshotFolder = new File(SCREENSHOTS_PATH);
        if(!screenshotFolder.exists()){
            screenshotFolder.mkdirs();
        }
        try {
            //Save screenshot to file
            String fileName = scenarioName + "-" + DateUtils.getCurrentTime("yyyy-MM-dd HH.mm.ss");
            String filePath = REPORT_PATH.concat("screenshots/" + fileName +".png");
            ImageIO.write(screenshot.getImage(), "png", new File(filePath));
            //Append screenshot to report
            test.fail("Screenshot").addScreenCaptureFromPath("." + filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeReport(){
        extent.flush();
        INSTANCE = null;
    }
}
