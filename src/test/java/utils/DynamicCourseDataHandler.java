package utils;

public class DynamicCourseDataHandler {
    public static final String TIMESTAMP = "[timestamp]";
    private String childrenFolderName;
    private String courseName;
    private String courseCode;
    private String tag;
    private String duplicatedCourseName;
    private String termName;
    private String termSisId;
    private String termStartDate;
    private String termEndDate;

    private static DynamicCourseDataHandler INSTANCE = null;

    private synchronized static void createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DynamicCourseDataHandler();
        }
    }

    public static DynamicCourseDataHandler getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public synchronized static void deleteInstance(){
        INSTANCE = null;
    }

    public String getChildrenFolderName() {
        return childrenFolderName;
    }

    public void setChildrenFolderName(String childrenFolderName) {
        this.childrenFolderName = childrenFolderName;
    }

    public String setRandomChildrenFolderName(String parentFolder) {
        if(parentFolder.contains(TIMESTAMP)){
            this.childrenFolderName = parentFolder.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.childrenFolderName = parentFolder.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.childrenFolderName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setRandomCourseName(String courseName) {
        if(courseName.contains(TIMESTAMP)){
            this.courseName = courseName.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.courseName = courseName.concat(String.valueOf(System.currentTimeMillis()));
        }
    }

    public String getDuplicatedCourseName() {
        return duplicatedCourseName;
    }

    public void setRandomDuplicatedCourseName(String duplicatedNewName) {
        if(duplicatedNewName.contains(TIMESTAMP)){
            this.duplicatedCourseName = duplicatedNewName.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.duplicatedCourseName = duplicatedNewName.concat(String.valueOf(System.currentTimeMillis()));
        }
    }

    public String setRandomCourseCode(String courseCode){
        if(courseCode.contains(TIMESTAMP)){
            this.courseCode = courseCode.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.courseCode = courseCode.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.courseCode;
    }

    public String getCourseCode(){
        return courseCode;
    }

    public String setRandomTag(String tag){
        if(tag.contains(TIMESTAMP)){
            this.tag = tag.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.tag = tag.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.tag;
    }

    public String setRandomTermName(String termName){
        if(termName.contains(TIMESTAMP)){
            this.termName = termName.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.termName = termName.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.termName;
    }

    public String getTermName() {
        return termName;
    }

    public String setRandomSisId(String termSisId){
        if(termSisId.contains(TIMESTAMP)){
            this.termSisId = termSisId.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.termSisId = termSisId.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.termSisId;
    }

    public String getTermSisId() {
        return termSisId;
    }

    public String setRandomTermStartDate(String termStartDate){
        if(termStartDate.contains(TIMESTAMP)){
            this.termStartDate = termStartDate.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.termStartDate = termStartDate.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.termStartDate;
    }

    public String getTermStartDate() {
        return termStartDate;
    }

    public String setRandomTermEndDate(String termEndDate){
        if(termEndDate.contains(TIMESTAMP)){
            this.termEndDate = termEndDate.replace(TIMESTAMP, String.valueOf(System.currentTimeMillis()));
        }
        else{
            this.termEndDate = termEndDate.concat(String.valueOf(System.currentTimeMillis()));
        }
        return this.termEndDate;
    }

    public String getTermEndDate() {
        return termEndDate;
    }

}