package utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import constants.TestModes;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class WebDriverFactory {
    private static final Config CONFIG = ConfigFactory.load("configuration.conf");

    public static WebDriver createWebDriver(String testMode) throws MalformedURLException {
        WebDriverFactory webDriverFactory = new WebDriverFactory();
        switch (testMode){
            case TestModes.LOCAL:
                return webDriverFactory.createLocalWebDriver();
            case TestModes.LOCAL_DEBUG:
                return webDriverFactory.createLocalDebugWebDriver();
            case TestModes.CROSSBROWSER:
                return webDriverFactory.createCrossbrowserWebDriver();
            case TestModes.CONTAINER:
                return webDriverFactory.createContainerWebDriver();
        }
        throw new IllegalArgumentException(String.format("The WebDriver mode %s is not implemented", testMode));
    }

    private WebDriver createLocalWebDriver(){
        //Set new driver
        String browser = CONFIG.getString("browser");
        switch (browser) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--window-size=1920,1080");
                chromeOptions.addArguments("--verbose");
                if(CONFIG.getBoolean("headless")) {
                    chromeOptions.addArguments("--headless");
                }
                return new ChromeDriver(chromeOptions);
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                return new FirefoxDriver(firefoxOptions);
            case "ie":
                WebDriverManager.iedriver().setup();
                InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                internetExplorerOptions.setCapability("ignoreZoomSetting", true);
                System.setProperty("webdriver.ie.driver", "/");
                return new InternetExplorerDriver(internetExplorerOptions);
            case "edge":
                WebDriverManager.edgedriver().setup();
                EdgeOptions edgeOptions = new EdgeOptions();
                return new EdgeDriver(edgeOptions);
            default:
                throw new IllegalArgumentException(String.format("The browser %s not supported", browser));
        }
    }

    private WebDriver createLocalDebugWebDriver() throws MalformedURLException {
        //If session file exists, load driver session data
        HashMap<String, String> webdriverSession = JsonUtils.getWebDriverSessionConfigFile();
        if(webdriverSession != null){
            URL url = new URL(webdriverSession.get("url"));
            return ReuseRemoteWebDriver.reconnectToSession(webdriverSession.get("sessionId"), url);
        }
        else{
            //Create new session
            WebDriver driver = createLocalWebDriver();
            //Save session data to file
            RemoteWebDriver remoteWebDriver = (RemoteWebDriver) driver;
            HttpCommandExecutor httpCommandExecutor = (HttpCommandExecutor) remoteWebDriver.getCommandExecutor();
            String url = httpCommandExecutor.getAddressOfRemoteServer().toString();
            SessionId sessionId = remoteWebDriver.getSessionId();
            JsonUtils.createWebDriverSessionConfigFile(url, sessionId.toString());
            return driver;
        }
    }

    private WebDriver createCrossbrowserWebDriver() throws MalformedURLException {
        String crossBrowserUsername = CONFIG.getString("crossBrowserUsername");
        String crossBrowserAuthkey = CONFIG.getString("crossBrowserAuthkey");
        String testScore = "unset";

        DesiredCapabilities caps = new DesiredCapabilities();
        HashMap<String, String> browserConfig = JsonUtils.findBrowserJsonToHashMap();

        for (String configKey : browserConfig.keySet()) {
            caps.setCapability(configKey, browserConfig.get(configKey));
        }
        caps.setCapability("record_video", "true");
        return new RemoteWebDriver(new URL("http://" + crossBrowserUsername + ":" + crossBrowserAuthkey +"@hub-cloud.crossbrowsertesting.com:80/wd/hub"), caps);
    }

    private WebDriver createContainerWebDriver() throws MalformedURLException {
        ChromeOptions chromeOptions = new ChromeOptions();
        if(CONFIG.getBoolean("headless")) {
            chromeOptions.addArguments("--headless");
        }
        String seleniumContainerIp;
        try {
            seleniumContainerIp = ConsoleCommandExecutor.resolveDomainToIp(CONFIG.getString("seleniumContainerName"));
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Could not execute command to find the IP of the selenium server container");
        }
        String webDriverUrl = "http://" + seleniumContainerIp + ":4444/wd/hub";
        System.out.println("Connecting to container WebDriver on URL " + webDriverUrl);
        return new RemoteWebDriver(new URL(webDriverUrl), chromeOptions);
    }

}
