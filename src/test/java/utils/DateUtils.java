package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    private static final String GENERIC_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String getCurrentTime(){
        return getCurrentTime(GENERIC_DATE_FORMAT);
    }

    public static String getCurrentTime(String format){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime now = LocalDateTime.now();
        return formatter.format(now);
    }

    public static String getCurrentMonthDateByDay(String format, String dayOfMonth) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime date = LocalDateTime.now();
        date = date.withDayOfMonth(Integer.parseInt(dayOfMonth));
        return formatter.format(date);
    }
}
