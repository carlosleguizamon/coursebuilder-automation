package utils;

import io.cucumber.java.Status;
import ru.yandex.qatools.ashot.Screenshot;

public interface Report {
    void addMethodResult(String methodName, Status result);
    void log(String title, String content);
    void appendScreenshot(String scenarioName, Screenshot screenshot);
    void closeReport();
}
