package pageobjects;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.lang3.NotImplementedException;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.Locator;
import utils.ExtentReport;
import utils.JsonUtils;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.List;
import java.util.function.Function;

public class BasePage {

    protected static final Config CONFIG = ConfigFactory.load("configuration.conf");

    protected WebDriver driver;
    protected String basePage;
    protected int pause = CONFIG.getInt("pause");
    protected String environment = CONFIG.getString("environment");

    public BasePage(WebDriver driver) {
        this.driver = driver;
        driver.manage().window().maximize();
        PageFactory.initElements(driver, this);
    }

    public void goToPage(String uri) {
        try {
            this.driver.navigate().to(new URI(this.basePage).resolve(uri).toURL());
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    protected WebElement waitElementToBeClickeable(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, pause);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement waitElementToBeClickeable(By locator){
        WebDriverWait wait = new WebDriverWait(driver, pause);
        return wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected WebElement waitElementToBePresent(WebElement element){
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(pause))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(WebDriverException.class);
        wait.until((Function<WebDriver, Boolean>) driver -> element.isDisplayed());
        return element;
    }

    protected WebElement waitElementToBePresent(WebElement element, By.ByXPath relativeLocator){
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(pause))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        wait.until((Function<WebDriver, Boolean>) driver -> element.findElement(relativeLocator).isDisplayed());
        return element;
    }

    protected WebElement waitElementTextLengthToBeMoreThan(By locator, int length){
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(pause))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        wait.until((Function<WebDriver, Boolean>) driver -> driver.findElement(locator).getText().length() > length);
        return driver.findElement(locator);
    }

    protected WebElement waitElementTextLengthToBeMoreThan(WebElement webElement, int length){
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(pause))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        wait.until((Function<WebDriver, Boolean>) driver -> webElement.getText().length() > length);
        return webElement;
    }

    protected WebElement waitElementToBePresent(By locator){
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(pause))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        wait.until((Function<WebDriver, Boolean>) driver -> driver.findElement(locator).isDisplayed());
        return driver.findElement(locator);
    }

    protected List<WebElement> waitElementsToBePresent(By locator){
        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(pause))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        wait.until((Function<WebDriver, Boolean>) driver -> driver.findElement(locator) != null);
        return driver.findElements(locator);
    }

    protected WebElement highlightElement(WebElement element){
        ((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid red'", element);
        return element;
    }

    /**
     * Checks if an element is displayed. The element can be a WebElement or a By class, otherwise will throw NotImplementedException
     *
     * @param element description of the exception
     * @param elementDescription description of the element
    */
    protected boolean isElementDisplayedCatchNotFoundException(Object element, String elementDescription){
        ExtentReport.log(String.format("Trying to find the element '%s'", elementDescription));

        //Check if element is displayed
        boolean isDisplayed = false;
        boolean classImplemented = false;
        try{
            if(element.getClass().getSimpleName().equals(WebElement.class.getSimpleName()) ||
                    element.getClass().getSuperclass().getSimpleName().equals(Proxy.class.getSimpleName())) {
                isDisplayed = waitElementToBePresent((WebElement) element).isDisplayed();
                classImplemented = true;
            }
            if(element.getClass().getSuperclass().getSimpleName().equals(By.class.getSimpleName())) {
                isDisplayed = waitElementToBePresent((By) element).isDisplayed();
                classImplemented = true;
            }
            if(!classImplemented){
                throw new NotImplementedException(String.format("The class %s has no implementation yet", element.getClass()));
            }
        } catch (NoSuchElementException | TimeoutException ex){
            ExtentReport.log(String.format("Could not find the element '%s'", elementDescription));
            return false;
        }

        //Log the result
        if(isDisplayed){
            ExtentReport.log(String.format("The element '%s' was displayed", elementDescription));
        }
        else{
            ExtentReport.log(String.format("The element '%s' was not displayed", elementDescription));
        }

        return isDisplayed;
    }

    protected WebElement waitElementToBeVisible(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, pause);
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    protected void waitElementToBeInvisible(By locator){
        WebDriverWait wait = new WebDriverWait(driver, pause);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    protected List<WebElement> waitNumberOfElementsToBeMoreThan(By locator, Integer number){
        WebDriverWait wait = new WebDriverWait(driver, pause);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(locator, number));
        return driver.findElements(locator);
    }

    protected List<WebElement> waitNumberOfElementsToBe(By locator, Integer number){
        WebDriverWait wait = new WebDriverWait(driver, pause);
        wait.until(ExpectedConditions.numberOfElementsToBe(locator, number));
        return driver.findElements(locator);
    }

    protected WebElement scrollIntoView(WebElement element, boolean alignToTop){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(arguments[1]);", element, alignToTop);
        return element;
    }

    protected void scrollWindow(int x, int y){
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(arguments[0], arguments[1]);", x, y);
    }

    protected void clickWithJS(WebElement el){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", el);
    }

    //Useful to avoid other browser tasks to interrupt the operation
    protected void sendKeysWithJS(WebElement el, String text){
        ((JavascriptExecutor) driver).executeScript("arguments[0].value = arguments[1]", el, text);
    }

    protected void sendKeysToReactInput(WebElement el, String text){
        ((JavascriptExecutor) driver).executeScript(
                "function setNativeValue(element, value) {\n" +
                    "const valueSetter = Object.getOwnPropertyDescriptor(element, 'value').set;\n" +
                    "const prototype = Object.getPrototypeOf(element);\n" +
                    "const prototypeValueSetter = Object.getOwnPropertyDescriptor(prototype, 'value').set;\n" +
                    "if (valueSetter && valueSetter !== prototypeValueSetter) {\n" +
                        "prototypeValueSetter.call(element, value);\n" +
                    "} else {\n" +
                        "valueSetter.call(element, value);\n" +
                    "}\n" +
                "}" +
                "" +
                "setNativeValue(arguments[0], arguments[1]);\n" +
                "arguments[0].dispatchEvent(new Event('input', { bubbles: true }));",
                el, text);
    }

    protected void removeHtmlElementWithJS(WebElement el){
        ((JavascriptExecutor) driver).executeScript("arguments[0].remove();", el);
    }

    protected boolean isRadioButtonInputChecked(WebElement radioButtonInput) {
        return (boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].checked", radioButtonInput);
    }

    public String getParameterValueFromUrl(String parameterKey){
        String value = null;
        String url = driver.getCurrentUrl();
        String allParameters = url.split("\\?")[1];
        String[] parameters = allParameters.split("&");
        for(String parameter: parameters){
            if(parameter.split("=")[0].equals(parameterKey)){
                value = parameter.split("=")[1];
                break;
            }
        }
        return value;
    }

    protected void clickAssertAlertButton() {
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

}
