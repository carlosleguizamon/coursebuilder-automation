package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

public class CalendarBasedFramework14WeeksTemplatePage extends TemplateBasePage {

    private final int NUMBER_OF_MODULES = 14;

    public CalendarBasedFramework14WeeksTemplatePage(WebDriver driver){
        super(driver);
    }

    @Override
    public boolean isPage() {
        try{
            waitNumberOfElementsToBeMoreThan(new By.ByXPath(navModuleMapLocator), NUMBER_OF_MODULES);
        } catch (TimeoutException ex){
            //Refresh the page and try again
            driver.navigate().refresh();
            waitNumberOfElementsToBeMoreThan(new By.ByXPath(navModuleMapLocator), NUMBER_OF_MODULES);
        }
        return true;
    }

}
