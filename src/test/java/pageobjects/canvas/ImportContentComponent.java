package pageobjects.canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

import static org.openqa.selenium.By.xpath;

public class ImportContentComponent extends BasePage {

    @FindBy(css = "button[aria-label='Import course']")
    private WebElement importButton;

    private static final String COLUMN_INPUT_XPATH = "//div[@id='select-import-source-course-table']//div[contains(@class, '--data-table-header-cell')]//button[contains(text(), '%s')]/../..//input";
    private static final String COLUMN_NAME_COURSES_XPATH = "//div[@id='select-import-source-course-table']//div[contains(@class, '--data-table-body-cell column-name')][contains(text(), '%s')]";
    private static final String COLUMN_RADIOBUTTON_XPATH = "//div[@id='select-import-source-course-table']//div[@class='pinned-left-columns']//div[@aria-rowindex='%d']//button";

    public ImportContentComponent(WebDriver driver){
        super(driver);
    }

    public void filter(String columnName, String textFilter) {
        WebElement columnInput = waitElementToBePresent(xpath(String.format(COLUMN_INPUT_XPATH, columnName)));
        columnInput.sendKeys(textFilter);
    }

    private WebElement getCourseCellByName(String courseName){
        return waitElementToBePresent(xpath(String.format(COLUMN_NAME_COURSES_XPATH, courseName)));
    }

    private void checkCourseByRowIndex(Integer rowIndex){
        waitElementToBePresent(xpath(String.format(COLUMN_RADIOBUTTON_XPATH, rowIndex))).click();
    }

    public void checkCourseByName(String courseName) {
        WebElement courseNameCell = getCourseCellByName(courseName);
        int courseRowIndex = Integer.parseInt(courseNameCell.getAttribute("aria-rowindex"));
        checkCourseByRowIndex(courseRowIndex);
    }

    public void clickImportButton() {
        waitElementToBeClickeable(importButton).click();
    }
}


