package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CalendarBasedFramework7WeeksTemplatePage extends TemplateBasePage {

    private final int NUMBER_OF_MODULES = 7;

    public CalendarBasedFramework7WeeksTemplatePage(WebDriver driver){
        super(driver);
    }

    @Override
    public boolean isPage() {
        waitNumberOfElementsToBeMoreThan(new By.ByXPath(navModuleMapLocator), NUMBER_OF_MODULES);
        return true;
    }

}
