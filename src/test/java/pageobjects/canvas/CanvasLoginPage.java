package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;
import utils.ExtentReport;

public class CanvasLoginPage extends BasePage {
    private final String emailLocatorId = "pseudonym_session_unique_id";

    @FindBy(id = emailLocatorId)
    private WebElement inputEmail;

    @FindBy(id = "pseudonym_session_password")
    private WebElement inputPassword;

    @FindBy(xpath = "//form[@id='login_form']//button[@type='submit']")
    private WebElement buttonLogin;

    public CanvasLoginPage(WebDriver driver){
        super(driver);
    }

    public void goToPage(){
        super.basePage = CONFIG.getString("canvasLoginPage");
        this.driver.navigate().to(basePage);
    }

    public void isPage(){
        ExtentReport.log("Waiting for input email element to be present on Canvas page");
        waitElementToBeClickeable(inputEmail);
    }

    public void login(String email, String password) {
        ExtentReport.log("Trying to log into Canvas page");
        inputEmail.sendKeys(email);
        inputPassword.sendKeys(password);
        buttonLogin.click();
        ExtentReport.log("Waiting for email input element to disappear");
        waitElementToBeInvisible(new By.ById(emailLocatorId));
    }
}