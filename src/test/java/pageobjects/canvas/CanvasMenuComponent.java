package pageobjects.canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;
import utils.ExtentReport;

public class CanvasMenuComponent extends BasePage {

    @FindBy(xpath = "//a[contains(text(), 'CourseBuilder')]")
    private WebElement anchorCoursebuilder;

    @FindBy(css = "ul#section-tabs li a.settings")
    private WebElement anchorSettings;

    public CanvasMenuComponent(WebDriver driver){
        super(driver);
    }

    public void clickCourseBuilderSection() {
        ExtentReport.log("Trying to click Course Builder section");
        waitElementToBeClickeable(anchorCoursebuilder).click();
    }

    public void clickSettingsSection(){
        ExtentReport.log("Trying to click Settings section");
        waitElementToBeClickeable(anchorSettings).click();
    }
}