package pageobjects.canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;
import utils.ExtentReport;

public class CanvasLeftHeaderComponent extends BasePage {

    @FindBy(css = ".coursebuilder-launch-button")
    private WebElement anchorCourseBuilder;

    public CanvasLeftHeaderComponent(WebDriver driver){
        super(driver);
    }

    public void clickCourseBuilder() {
        ExtentReport.log("Trying to click Course Builder tag");
        waitElementToBeClickeable(anchorCourseBuilder).click();
    }
}