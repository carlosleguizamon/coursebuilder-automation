package pageobjects.canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;
import utils.ExtentReport;

public class TemplateBasePage extends BasePage {

    @FindBy(css = "aside[id='right-side'] i[class='icon-import']")
    protected WebElement iconImportExistingContent;

    protected final String navModuleMapLocator = "//nav[@id='en-module-map']/div";

    public TemplateBasePage(WebDriver driver){
        super(driver);
    }

    public boolean isPage() {
        ExtentReport.log("Trying to find the icon Import Existing Content on Canvas Course page");
        return waitElementToBePresent(iconImportExistingContent).isDisplayed();
    }

}
