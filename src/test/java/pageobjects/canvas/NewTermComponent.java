package pageobjects.canvas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

import java.util.List;

public class NewTermComponent extends BasePage {

    @FindBy(xpath = "//iframe[@id='tool_content']")
    private WebElement iframeCourseBuilder;

    @FindBy(css = "button[id*='start-date-date-picker-button']")
    private WebElement startDateButton;

    @FindBy(css = "button[id*='end-date-date-picker-button']")
    private WebElement endDateButton;

    @FindBy(css = "div.calendar-time-container")
    private WebElement datePickerContainer;

    @FindBy(css = "button[data-testid='datetime-popover-close-button']")
    private WebElement datePickerCloseButton;

    @FindBy(id = "new-term-name")
    private WebElement newTermName;

    @FindBy(id = "new-term-sis-id")
    private WebElement newTermSisId;

    @FindBy(id = "new-term-start-at-date-picker-button")
    private WebElement newTermStartDatePickerButton;

    @FindBy(id = "new-term-end-at-date-picker-button")
    private WebElement newTermEndDatePickerButton;

    @FindBy(css = "div[class*='DayPicker-Day'][aria-disabled='false']")
    private List<WebElement> datePickerDaysOfMonth;

    @FindBy(css = "button[data-testid='new-term-popover-submit-button']")
    private WebElement newTermSubmitButton;

    @FindBy(id = "new-term-start-at-time")
    private WebElement newTermStartAtTimeInput;

    @FindBy(id = "new-term-end-at-time")
    private WebElement newTermEndAtTimeInput;

    private final static String DEFAULT_HOUR = "12:00";

    public NewTermComponent(WebDriver driver){
        super(driver);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(waitElementToBePresent(iframeCourseBuilder));
    }

    public void clickStartDateButton() {
        waitElementToBeClickeable(startDateButton).click();
    }

    public void clickEndDateButton() {
        waitElementToBeClickeable(endDateButton).click();
    }

    public boolean isDatePickerDisplayed() {
        return isElementDisplayedCatchNotFoundException(datePickerContainer, "Date picker container");
    }

    public void clickDatePickerCloseButton() {
        waitElementToBeClickeable(datePickerCloseButton).click();
    }

    public void fillInNewTermForm(String termName, String sisId, String startDayOfMonth, String endDayOfMonth) {
        waitElementToBePresent(newTermName).clear();
        newTermName.sendKeys(termName);
        newTermSisId.clear();
        newTermSisId.sendKeys(sisId);
        //Start date
        newTermStartDatePickerButton.click();
        datePickerDaysOfMonth.stream().filter(we -> we.getText().equals(startDayOfMonth)).findFirst().orElse(null).click();
        sendKeysToReactInput(newTermStartAtTimeInput, DEFAULT_HOUR);
        datePickerCloseButton.click();
        //End date
        newTermEndDatePickerButton.click();
        datePickerDaysOfMonth.stream().filter(we -> we.getText().equals(endDayOfMonth)).findFirst().orElse(null).click();
        sendKeysToReactInput(newTermEndAtTimeInput, DEFAULT_HOUR);
        datePickerCloseButton.click();
    }

    public void clickSubmitButton() {
        waitElementToBeClickeable(newTermSubmitButton).click();
    }

    public boolean isSubmitButtonDisplayed() {
        return isElementDisplayedCatchNotFoundException(newTermSubmitButton, "New Term Create/Update (submit) button");
    }
}


