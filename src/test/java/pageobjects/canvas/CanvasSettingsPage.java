package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pageobjects.BasePage;
import utils.DateUtils;

import java.util.List;

public class CanvasSettingsPage extends BasePage {

    public static final String DATE_FORMAT_DAY_ONLY = "MMM d, yyyy";
    private static final String DATE_FORMAT_FULL = "MMM d, yyyy 'at' HHa";
    private static final String DATE_HOUR_SEPARATOR = " at";

    @FindBy(id = "course_enrollment_term_id")
    private WebElement termDropdown;

    @FindBy(id = "course_start_at")
    private WebElement startDateInput;

    @FindBy(id = "course_conclude_at")
    private WebElement endDateInput;

    @FindBy(css = FLASH_MESSAGES_CSS)
    private List<WebElement> flashMessages;

    @FindBy(css = "div.form-actions button[type='submit']")
    private WebElement updateCourseDetailsButton;

    private static final String FLASH_MESSAGES_CSS = "#flash_message_holder li";

    public CanvasSettingsPage(WebDriver driver){
        super(driver);
    }

    public String getTermName() {
        Select select = new Select(waitElementToBePresent(termDropdown));
        return select.getAllSelectedOptions().get(0).getText();
    }

    public void setStartDateForCurrentMonth(String dayOfMonth){
        String date = DateUtils.getCurrentMonthDateByDay(DATE_FORMAT_FULL, dayOfMonth);
        startDateInput.sendKeys(date);
    }

    public void setEndDateForCurrentMonth(String dayOfMonth){
        String date = DateUtils.getCurrentMonthDateByDay(DATE_FORMAT_FULL, dayOfMonth);
        endDateInput.sendKeys(date);
    }

    public String getStartDate(){
        return startDateInput.getAttribute("value").split(DATE_HOUR_SEPARATOR)[0];
    }

    public String getEndDate(){
        return endDateInput.getAttribute("value").split(DATE_HOUR_SEPARATOR)[0];
    }

    public boolean isMessageDisplayed(String message) {
        try{
            waitNumberOfElementsToBeMoreThan(By.cssSelector(FLASH_MESSAGES_CSS), 0);
        } catch (TimeoutException e){
            return false;
        }
        for(WebElement flashMessage: flashMessages){
            if(flashMessage.getText().contains(message)){
                return true;
            }
        }
        return false;
    }

    public void clickUpdateCourseDetailsButton() {
        waitElementToBeClickeable(updateCourseDetailsButton).click();
    }
}