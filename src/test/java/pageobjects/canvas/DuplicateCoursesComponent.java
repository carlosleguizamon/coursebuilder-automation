package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pageobjects.BasePage;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class DuplicateCoursesComponent extends BasePage {

    @FindBy(xpath = "//iframe[@id='tool_content']")
    private WebElement iframeCourseBuilder;

    @FindBy(css = "button[data-testid='duplicate-courses-submit-button']")
    private WebElement buttonDuplicate;

    @FindBy(css = COURSE_DUPLICATE_DESTINATION_ACCOUNT_DROPDOWN_CSS)
    private WebElement destinationAccountDropdown;

    @FindBy(css = COURSE_DUPLICATE_DESTINATION_TERM_DROPDOWN_CSS)
    private WebElement destinationTermDropdown;

    @FindBy(css = COURSE_DUPLICATE_CURRENT_DROPDOWN_OPTIONS_CSS)
    private List<WebElement> currentDropdownOptions;

    @FindBy(id = "new-account-popover-name")
    private WebElement createNewAccountNameInput;

    @FindBy(id = "new-account-popover-parent-account")
    private WebElement createNewAccountParentAccountDropdown;

    @FindBy(css = "button[data-testid='new-account-popover-ok-button']")
    private WebElement newAccountFormCreateButton;

    private final String COURSE_NAME_XPATH = "//div[contains(@class, 'course-heading-description')]/h4/following-sibling::p[contains(text(), '%s')]";
    private final String COURSE_ITEM_XPATH = COURSE_NAME_XPATH + "/../../parent::div";
    private final String COURSE_DUPLICATE_QUANTITY_INPUT_CSS = "input[name='quantity']";
    private final String COURSE_DUPLICATE_AUTO_SUFFIX_SELECT_CSS = "select[name='suffix']";
    private final String COURSE_DUPLICATE_DESTINATION_ACCOUNT_DROPDOWN_CSS = "div[data-testid='account-select-wrapper']";
    private final String COURSE_DUPLICATE_DESTINATION_TERM_DROPDOWN_CSS = "div[data-testid='term-select-wrapper']";
    private final String COURSE_DUPLICATE_CURRENT_DROPDOWN_OPTIONS_CSS = "div[class*='option'][id*='react-select']";
    private final String COURSE_ADD_BUTTON_CSS = "button.source-course-add-button";
    private final String COURSE_REMOVE_BUTTON_CSS = "button[aria-label='remove']";
    private final String COURSE_DUPLICATE_NAMES_INPUT_XPATH = "//input[contains(@name, 'name')]";
    private final String COURSE_DUPLICATE_NAME_INPUT_XPATH = COURSE_DUPLICATE_NAMES_INPUT_XPATH + "[contains(@value, '%s')]";
    private final String COURSE_DUPLICATE_ITEM_RELATIVE_XPATH = "../../../parent::li[contains(@class, 'duplicated-course-item')]";

    public DuplicateCoursesComponent(WebDriver driver){
        super(driver);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(waitElementToBePresent(iframeCourseBuilder));
    }

    public boolean isCourseDisplayed(String courseName) {
        return isElementDisplayedCatchNotFoundException(By.xpath(String.format(COURSE_NAME_XPATH, courseName)), "Course name on Duplicate Courses page");
    }

    public List<String> getDuplicatedNames() {
        List<WebElement> duplicatedNames = driver.findElements(By.xpath(COURSE_DUPLICATE_NAMES_INPUT_XPATH));
        if(duplicatedNames.isEmpty()){
            return Collections.emptyList();
        }
        return duplicatedNames.stream().map(we -> we.getAttribute("value")).collect(Collectors.toList());
    }

    private WebElement getCourseItem(String courseName){
        return waitElementToBePresent(By.xpath(String.format(COURSE_ITEM_XPATH, courseName)));
    }

    private WebElement getDuplicatedItem(String duplicateName){
        List<WebElement> duplicatedNameInputs = driver.findElements(By.xpath(COURSE_DUPLICATE_NAMES_INPUT_XPATH));
        for(WebElement duplicateNameInput: duplicatedNameInputs){
            if(duplicateNameInput.getAttribute("value").contains(duplicateName)){
                return duplicateNameInput.findElement(By.xpath(COURSE_DUPLICATE_ITEM_RELATIVE_XPATH));
            }
        }
        throw new NoSuchElementException(String.format("Duplicated course item with name '%s' could not be found", duplicateName));
    }

    public void selectDuplicatesForCourse(int duplicates, String courseName) {
        WebElement courseItem = getCourseItem(courseName);
        WebElement duplicatesInput = courseItem.findElement(By.cssSelector(COURSE_DUPLICATE_QUANTITY_INPUT_CSS));
        duplicatesInput.sendKeys(String.valueOf(duplicates));
    }

    public void selectAutoSuffixForCourse(String autoSuffix, String courseName) {
        WebElement courseItem = getCourseItem(courseName);
        WebElement autoSuffixWebElement = courseItem.findElement(By.cssSelector(COURSE_DUPLICATE_AUTO_SUFFIX_SELECT_CSS));
        Select autoSuffixSelect = new Select(autoSuffixWebElement);
        autoSuffixSelect.selectByVisibleText(autoSuffix);
    }

    public void clickAddButtonForCourse(String courseName) {
        WebElement courseItem = getCourseItem(courseName);
        WebElement duplicatesInput = courseItem.findElement(By.cssSelector(COURSE_ADD_BUTTON_CSS));
        duplicatesInput.click();
    }

    public void clickDuplicateButton() {
        waitElementToBeClickeable(buttonDuplicate).click();
    }

    public void renameCourseDuplicate(String duplicatedCourseName, String suffix, String duplicateNewName) {
        WebElement courseItem = getCourseItem(duplicatedCourseName);
        WebElement duplicateInput = courseItem.findElement(By.xpath(String.format(COURSE_DUPLICATE_NAME_INPUT_XPATH, suffix)));
        duplicateInput.clear();
        duplicateInput.sendKeys(duplicateNewName);
    }

    public void clickDeleteButtonForCourseDuplicate(String duplicateName){
        WebElement duplicatedItem = getDuplicatedItem(duplicateName);
        WebElement deleteButton = duplicatedItem.findElement(By.cssSelector(COURSE_REMOVE_BUTTON_CSS));
        Actions actions = new Actions(driver);
        actions.moveToElement(deleteButton).build().perform();
        deleteButton.click();
    }

    public void clickDeleteButtonForCourseHost(String courseName){
        WebElement courseItem = getCourseItem(courseName);
        courseItem.findElement(By.cssSelector(COURSE_REMOVE_BUTTON_CSS)).click();
    }

    public void clickDestinationAccountDropdown(){
        waitElementToBeClickeable(destinationAccountDropdown).click();
    }

    public void clickDestinationTermDropdown(){
        waitElementToBeClickeable(destinationTermDropdown).click();
    }

    public List<String> getCurrentDropdownValues(){
        waitNumberOfElementsToBeMoreThan(By.cssSelector(COURSE_DUPLICATE_CURRENT_DROPDOWN_OPTIONS_CSS), 0);
        return currentDropdownOptions.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public void clickCurrentDropdownValue(String optionText){
        waitNumberOfElementsToBeMoreThan(By.cssSelector(COURSE_DUPLICATE_CURRENT_DROPDOWN_OPTIONS_CSS), 0);
        WebElement option = currentDropdownOptions.stream().filter(we -> we.getText().contains(optionText)).findFirst().orElse(null);
        option.click();
    }

    public String getSelectedDestinationTerm() {
        return waitElementToBePresent(destinationTermDropdown).getText();
    }

    public void fillInCreateNewAccountForm(String newAccountName, String parentFolderName) {
        waitElementToBePresent(createNewAccountNameInput).sendKeys(newAccountName);
        createNewAccountParentAccountDropdown.click();
        clickCurrentDropdownValue(parentFolderName);
    }

    public void clickCreateNewAccountFormCreateButton() {
        newAccountFormCreateButton.click();
    }
}


