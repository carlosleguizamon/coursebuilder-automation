package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.BasePage;

public class CanvasCoursesPage extends BasePage {

    @FindBy(xpath = "//button[@aria-label='Create new course']")
    private WebElement buttonCreateNewCourse;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement inputSearchCourse;

    private String courseLink = "//tbody//a[contains(text(),'%s')]";

    public CanvasCoursesPage(WebDriver driver){
        super(driver);
    }

    public void goToPage() {
        Integer accountNumber = CONFIG.getInt("everspringCourseBuilderAccountNumber");
        super.basePage = String.format(CONFIG.getString("canvasAccountPage"), accountNumber);
        this.driver.navigate().to(basePage);
    }

    public boolean isPage() {
        return buttonCreateNewCourse.isDisplayed();
    }

    public void search(String courseName) {
        inputSearchCourse.sendKeys(courseName);
        inputSearchCourse.submit();
    }

    public void clickCourseLink(String courseName) {
        waitElementToBeClickeable(new By.ByXPath(String.format(courseLink, courseName))).click();
    }
}