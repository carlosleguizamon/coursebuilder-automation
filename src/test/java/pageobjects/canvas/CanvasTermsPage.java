package pageobjects.canvas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pageobjects.BasePage;

import java.util.List;

public class CanvasTermsPage extends BasePage {

    public static final String DATE_FORMAT = "MMM d";
    private static final String DATE_HOUR_SEPARATOR = " at";

    @FindBy(css = TERM_TITLES_CSS)
    private List<WebElement> termTitles;

    private final static String TERM_CONTAINER_XPATH = "//td[@class='header']//div[@class='show_term']//span[@class='name'][text()='%s']/../../parent::tr[@class='term']";
    private final static String TERM_TITLES_CSS = "td.header div.show_term span.name";
    private final static String TERM_START_DATES_CSS = "span.start_at";
    private final static String TERM_END_DATES_CSS = "span.end_at";

    public CanvasTermsPage(WebDriver driver){
        super(driver);
    }

    public boolean isTermDisplayedByName(String termName) {
        waitNumberOfElementsToBeMoreThan(By.cssSelector(TERM_TITLES_CSS), 0);
        WebElement termTitle = termTitles.stream().filter(we -> we.getText().equals(termName)).findFirst().orElse(null);
        return termTitle != null;
    }

    private WebElement getTermContainer(String termName){
        return waitElementToBePresent(By.xpath(String.format(TERM_CONTAINER_XPATH, termName)));
    }

    public String getStartDate(String termName) {
        WebElement termContainer = getTermContainer(termName);
        String startDateText = termContainer.findElement(By.cssSelector(TERM_START_DATES_CSS)).getText();
        return startDateText.split(DATE_HOUR_SEPARATOR)[0];
    }

    public String getEndDate(String termName) {
        WebElement termContainer = getTermContainer(termName);
        String endDateText = termContainer.findElement(By.cssSelector(TERM_END_DATES_CSS)).getText();
        return endDateText.split(DATE_HOUR_SEPARATOR)[0];
    }
}