package pageobjects.canvas;

import com.aventstack.extentreports.utils.ExceptionUtil;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.BasePage;
import utils.ExtentReport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CanvasCourseBuilderComponent extends BasePage {

    @FindBy(xpath = "//iframe[@id='tool_content']")
    private WebElement iframeCourseBuilder;

    @FindBy(id = "function-button-add_courses")
    private WebElement buttonAddCourses;

    @FindBy(id = "function-button-add_account")
    private WebElement buttonAddAccount;

    @FindBy(xpath = "//span[contains(text(), 'Loading Courses')]")
    private WebElement pLoadingCourses;

    @FindBy(xpath = "//button[@aria-label='Accounts Tree View']")
    private WebElement buttonTreeView;

    @FindBy(xpath = "//button[@aria-label='Course List View']")
    private WebElement buttonListView;

    @FindBy(id = "quantity-input")
    private WebElement inputCoursesQuantity;

    @FindBy(xpath = "//button[@aria-label='Add courses']")
    private WebElement buttonAdd;

    @FindBy(id = "function-button-delete_unlocked")
    private WebElement buttonDeleteAccount;

    @FindBy(css = "a.account-link")
    private WebElement buttonGoToSubAccount;

    @FindBy(xpath = "//button[contains (@class,'sync-changes-button')]")
    private WebElement buttonSyncAllChanges;

    @FindBy(css = "footer button[aria-label='Apply Framework']")
    private WebElement buttonApply;

    @FindBy(css = "button[aria-label='Close modal']")
    private WebElement buttonCloseSyncStatusWindow;

    @FindBy(xpath = "//button[@aria-label='Check sync status']")
    private WebElement buttonCheckSyncStatus;

    @FindBy(id="function-button-delete_unlocked")
    private WebElement buttonBulkActionsDelete;

    @FindBy(id = "function-button-apply_framework")
    private WebElement buttonBulkActionsApplyFramework;

    @FindBy(xpath = "//button[contains(@class, 'course-menu-button')]")
    private WebElement buttonCourseSettings;

    @FindBy(id = "function-button-clear_selection")
    private WebElement buttonClearSelections;

    @FindBy(xpath = "//div[@ref='gridPanel']")
    private WebElement divGridPanel;

    @FindBy(css = "#oauth2_accept_form input[value='Authorize']")
    private WebElement buttonAuthorize;

    @FindBy(xpath = "//li[@data-testid='toggle-tab-sync-view']//button")
    private WebElement statusSyncLabel;

    @FindBy(xpath = "//button[contains(@data-testid,'toast-confirm-ok-button')]")
    private WebElement buttonAcceptSyncToast;

    @FindBy(xpath = "//*[@id='courses-list']/div[3]/div[1]/div[1]/div/label/button")
    private WebElement checkboxSelectAllCourses;

    @FindBy(xpath = "//button[@id='function-button-apply_framework']")
    private WebElement buttonApplyFramework;

    @FindBy(xpath = "//button[@aria-label='Show Sync Job Details']")
    private WebElement buttonShowSyncJobFirstRow;

    @FindBy(xpath = "//div[contains(@class,'sync-job-item')]//tr[1]//td[1]")
    private WebElement folderSyncCheckFirstRow ;

    @FindBy(css = "div#templates-list input")
    private List<WebElement> frameworkRadioButtonInputs;

    @FindBy(xpath = "//button[contains(@class,'selection-checkbox-button')]")
    private WebElement buttonSelectFirstFolder;

    @FindBy(xpath = "//button[@aria-label='Course Menu']")
    private WebElement firstCourseRadioButtons;

    @FindBy(xpath = "//button[@aria-label='Resize']")
    private WebElement resizeAccountsSectionButton;

    @FindBy(xpath = "//div[@class='resizable-column closed']")
    private WebElement accountSectionClosed;

    @FindBy(id = "courses-list-column-select")
    private WebElement buttonColumnsToDisplayDropdown;

    @FindBy(xpath = "//div[@data-testid='table-controls-container']//div[contains(@class, 'dropdown-menu')]")
    private WebElement columnsToDisplayDropdownContainer;

    @FindBy(xpath = "//div[@data-testid='table-controls-container']//div[contains(@class, 'dropdown-menu')]/button")
    private List<WebElement> columnsToDisplayDropdownButtons;

    @FindBy(id = "function-button-duplicate_courses")
    private WebElement buttonDuplicateCourses;

    @FindBy(id = "function-button-import_course")
    private WebElement buttonImportCourse;

    @FindBy(xpath = "//div[@class='popover-body']//input")
    private WebElement popoverInput;

    @FindBy(css = "button[data-testid='edit-single-value-submit-button']")
    private WebElement popoverSubmitButton;

    @FindBy(css = "button[data-testid='edit-new-term-button']")
    private WebElement editNewTermButton;

    private String OPTION_COURSE_TEMPLATE_XPATH = "//p[text()='%s']/parent::div/parent::div/preceding-sibling::div//label";
    private String COURSE_MENU_BUTTON_XPATH = "//button/span[text()='Course Menu']/parent::button";
    private final String INPUT_COURSE_COLUMN_XPATH = "//div[@class='table-header-center-container']//div[@role='columnheader']";
    private final String PARENT_FOLDER_XPATH = "//div[@class='account-item-description' and contains(text(),\"%s\")]/../../parent::li";
    private final String PARENT_FOLDER_OPTIONS_BUTTON_XPATH = PARENT_FOLDER_XPATH + "//button[@aria-label= 'Account Menu.']";
    private final String FOLDER_CHILDRENS_XPATH = ".//ul[@class='sub-accounts-list']";
    private final String CHILDREN_FOLDER_XPATH = ".//*[text()='%s']/../../parent::li";
    private final String UNSAVED_CHILDREN_FOLDER_XPATH = "//*[text()='%s']/../../../../parent::li";
    private final String TABLE_COURSES_CSS = "div.center-columns > div.--data-table-row";
    private final String TABLE_COURSES_CELLS_CSS = "div.center-columns div[aria-colindex='%d']";
    private final String TABLE_COURSES_LINK_CSS = "div[class*='--data-table-body-cell column-name'] a.course-external-link";
    private final String TABLE_COURSE_NAME_BUTTON_XPATH = "//div[contains(@class, '--data-table-body-cell column-name')]//button[text()='%s']";
    private final String TABLE_COURSE_TERM_CELL_CSS = "div[class*='--data-table-body-cell column-term']";
    private final String TABLE_COURSE_TERM_BUTTON_CSS = TABLE_COURSE_TERM_CELL_CSS + " button";
    private final String TABLE_COURSE_CODE_CELL_XPATH = ".//div[contains(@class, '--data-table-body-cell column-course_code')]";
    private final String TABLE_COURSE_CODE_BUTTON_XPATH = TABLE_COURSE_CODE_CELL_XPATH + "//button";
    private final String TABLE_COURSE_TAG_BUTTON_XPATH = ".//div[contains(@class, '--data-table-body-cell column-tags')]//button";
    private final String TABLE_COURSE_TAG_BUTTON_SPAN_XPATH = TABLE_COURSE_TAG_BUTTON_XPATH + "//span";
    private final String TABLE_SAVED_COURSE_XPATH = "//div[contains(@class, 'column-name')]//a[contains(text(),'%s')]/../../parent::div";
    private final String TABLE_SYNCING_COURSE_XPATH = "//div[contains(@class, 'column-name')]//div[contains(text(),'%s')]/../parent::div";
    private final String TABLE_SAVED_COURSE_TEMPLATE_BUTTON_XPATH = TABLE_SAVED_COURSE_XPATH + "/div/div/button";
    private final String TABLE_UNSAVED_COURSE_XPATH = "//div[contains(@class, 'column-name')]//button[contains(text(), '%s')]/../../../..";
    private final String FRAMEWORK_TEMPLATE_NAME_FROM_RADIO_BUTTON_INPUT_XPATH = "../label/span";
    private final String TABLE_UNSAVED_COURSE_CHECKBOX_CSS = "div[aria-colindex='1'][aria-rowindex='%s'] label";
    private final String TABLE_COURSE_CHECKBOX_CSS = "div.table-body-container div[aria-colindex='1'] input";
    private final String TABLE_UNSAVED_COURSE_MENU_BUTTON_CSS = "div[aria-colindex='2'][aria-rowindex='2'] button";
    private final String TABLE_COURSES_SYNCING_ICON_CONTAINER_CSS = "div[aria-colindex='3'][aria-rowindex='%s'] div[aria-label='SYNCING']";
    private final String TABLE_COURSE_CELL_STRING_TO_IGNORE = "Edit";
    private final String COLUMNS_TO_DISPLAY_DROPDOWN_BUTTON = "//div[@data-testid='table-controls-container']//button[contains(@class, 'column-select-button')][text()='%s']";
    private final String ACCEPT_POPUP_BUTTON_XPATH = "//button[contains(@data-testid, 'toast-confirm-ok-button')]";

    public CanvasCourseBuilderComponent(WebDriver driver){
        super(driver);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(waitElementToBePresent(iframeCourseBuilder));
    }

    public boolean isPage(){
        try{
            return waitElementToBePresent(buttonAddCourses).isDisplayed();
        } catch (TimeoutException ex){
            ExtentReport.log("The button 'Add Courses' was not found on Course Builder page");
            return false;
        }
    }

    public void clickAuthorizeButtonIfDisplayed(){
        try{
            waitElementToBeClickeable(buttonAuthorize).click();
        } catch (TimeoutException | NoSuchElementException ex){
            //Do nothing if the element is not displayed
        }
    }

    public void waitForCoursesToBeLoaded() {
        ExtentReport.log("Waiting for courses to be loaded");
        try{
            waitElementToBePresent(pLoadingCourses);

            int progress = 0;
            while (progress < 100 && isElementDisplayedCatchNotFoundException(pLoadingCourses, "Loading courses text")) {
                String strProgress = pLoadingCourses.getText().replaceAll("[^0-9]", "");
                if(!strProgress.isEmpty()){
                    progress = Integer.parseInt(strProgress);
                }
                ExtentReport.log(String.format("Waiting for load progress to be 100, current progress is %d", progress));
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (TimeoutException | NoSuchElementException ex){
            //Do nothing if the loading window was already closed
        }
        waitElementToBePresent(buttonTreeView);
    }

    public boolean isTreeViewButtonDisplayed(){
        return isElementDisplayedCatchNotFoundException(buttonTreeView, "Tree View button");
    }

    private WebElement getFolder(String folderName){
        return waitElementToBePresent(By.xpath(String.format(PARENT_FOLDER_XPATH, folderName)));
    }

    private WebElement getUnsavedFolder(String folderName){
        return waitElementToBePresent(By.xpath(String.format(UNSAVED_CHILDREN_FOLDER_XPATH, folderName)));
    }

    private WebElement getChildrenOptions(String parentFolderName, String childrenFolderName){
        WebElement folder = getChildrenFolder(parentFolderName, childrenFolderName);
        return folder.findElement(By.xpath("./div[2]"));
    }

    private WebElement getFolderChildrens(String folderName){
        return getFolder(folderName).findElement(By.xpath(FOLDER_CHILDRENS_XPATH));
    }

    private WebElement getChildrenFolder(String parentFolderName, String childrenFolderName){
        WebElement folders = getFolderChildrens(parentFolderName);
        return folders.findElement(By.xpath(String.format("./li//*[text()='%s']/parent::span/parent::div/parent::li", childrenFolderName)));
    }

    //Children folder html structure changes once the folder has been saved (submitted)
    private WebElement getSavedChildrenFolder(String parentFolderName, String childrenFolderName){
        WebElement folders = getFolderChildrens(parentFolderName);
        return folders.findElement(By.xpath(String.format(CHILDREN_FOLDER_XPATH, childrenFolderName)));
    }

    public void renameChildrenFolder(String defaultFolderName, String parentFolderName, String newFolderName) {
        WebElement inputFolderName = getFolderChildrens(parentFolderName).findElement(By.xpath(
                String.format(".//input[@value='%s']", defaultFolderName)
        ));
        waitElementToBeClickeable(inputFolderName);
        //Clear the text inside the element
        for(int i=0; i<defaultFolderName.length(); i++){
            inputFolderName.sendKeys(Keys.BACK_SPACE);
        }
        //Set new name
        inputFolderName.sendKeys(newFolderName);
        //Click input button and parent folder to apply changes
        inputFolderName.findElement(By.xpath("./..")).click();
        clickFolder(parentFolderName);
    }

    public void renameCourse(String newCourseName) {
        fillPopupInput(newCourseName);
    }

    public void renameCourseCode(String newCourseCode){
        fillPopupInput(newCourseCode);
    }

    public void addCourseTag(String tagName){
        fillPopupInput(tagName);
        popoverInput.sendKeys(Keys.ENTER); //To close the popup
    }

    private void fillPopupInput(String text){
        waitElementToBePresent(popoverInput);
        popoverInput.clear();
        popoverInput.sendKeys(text);
        popoverInput.sendKeys(Keys.ENTER);
    }

    //Children folder html structure changes once the folder has been saved (submitted)
    public void clickSavedChildrenFolder(String childrenFolderName, String parentFolderName) {
        ExtentReport.log(String.format("Trying to click the folder '%s'/'%s'", parentFolderName, childrenFolderName));
        waitElementToBeClickeable(getSavedChildrenFolder(parentFolderName, childrenFolderName)).click();
    }

    public void clickFolder(String folderName) {
        try{
            clickUnsavedFolder(folderName);
        } catch (TimeoutException | NoSuchElementException ex){
            //If the folder is saved, use the saved folder locator
            getFolder(folderName).click();
        }
    }

    public boolean isFolderDisplayed(String folderName){
        ExtentReport.log(String.format("Verifying if the folder '%s' is displayed", folderName));
        try{
            return getUnsavedFolder(folderName).isDisplayed();
        } catch (TimeoutException | NoSuchElementException ex){
            //If the folder is saved, use the saved folder locator
            try{
                return getFolder(folderName).isDisplayed();
            } catch (TimeoutException | NoSuchElementException e){
                //If none of the above methods worked, the folder is not displayed
                return false;
            }
        }
    }

    public void clickUnsavedFolder(String folderName){
        getUnsavedFolder(folderName).click();
    }

    public void clickAddAccountButton(){
        waitElementToBeClickeable(buttonAddAccount).click();
    }

    public void clickAddCoursesButton(){
        waitElementToBeClickeable(buttonAddCourses).click();
    }

    public void clickMenuButtonOverChildrenFolder(String childrenFolderName, String parentFolderName) {
        WebElement folderOptions = getChildrenOptions(parentFolderName, childrenFolderName);
        WebElement buttonOptions = folderOptions.findElement(By.xpath("./button//*[name()='svg' and @data-icon='ellipsis-v']/parent::button"));
        buttonOptions.click();
    }

    public void clickMenuButtonOverFolder(String folderName) {
        //Wait for the lateral window to be expanded
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            ExtentReport.log("An exception occurred when waiting for the lateral window to be expanded: " + ExceptionUtil.getStackTrace(e));
        }
        //Move to the folder element, so that the options menu button gets displayed
        WebElement folder = getFolder(folderName);
        Actions actions = new Actions(driver);
        actions.moveToElement(folder).perform();
        //Find and click the button
        WebElement folderOptionsButton = waitElementToBeClickeable(By.xpath(String.format(PARENT_FOLDER_OPTIONS_BUTTON_XPATH, folderName)));
        folderOptionsButton.click();
    }

    public void fillInCoursesToAdd(String coursesToAdd) {
        waitElementToBePresent(inputCoursesQuantity).sendKeys(coursesToAdd);
    }

    public void selectCourseTemplate(String courseTemplate) {
        ExtentReport.log(String.format("Trying to select the course template '%s'", courseTemplate));
        By courseTemplateOptionLocator = By.xpath(String.format(OPTION_COURSE_TEMPLATE_XPATH, courseTemplate));
        WebElement courseTemplateOption =  waitElementToBeClickeable(courseTemplateOptionLocator);
        scrollIntoView(courseTemplateOption, false);
        clickWithJS(courseTemplateOption);
    }

    private WebElement getSavedCourse(String courseName){
        return waitElementToBePresent(By.xpath(String.format(TABLE_SAVED_COURSE_XPATH, courseName)));
    }

    private WebElement getUnsavedCourse(String courseName){
        return waitElementToBePresent(By.xpath(String.format(TABLE_UNSAVED_COURSE_XPATH, courseName)));
    }

    private WebElement getCourse(String courseName){
        WebElement course;
        try{
            course = getSavedCourse(courseName);
        } catch (TimeoutException e){
            course = getUnsavedCourse(courseName);
        }
        return course;
    }

    private WebElement getSyncingCourse(String courseName){
        return waitElementToBePresent(By.xpath(String.format(TABLE_SYNCING_COURSE_XPATH, courseName)));
    }

    private WebElement getFirstDisplayedSavedCourse(String childrenFolderName, String parentFolderName){
        WebElement folder = getSavedChildrenFolder(parentFolderName, childrenFolderName);
        return folder.findElement(By.xpath("./following-sibling::ul//li"));
    }

    public void clickSavedCourseTemplateButton(String courseName) {
        ExtentReport.log("Trying to click the course template button");
        By courseTemplateButtonLocator = new By.ByXPath(String.format(TABLE_SAVED_COURSE_TEMPLATE_BUTTON_XPATH, courseName));
        WebElement courseTemplateButton = waitElementToBeClickeable(courseTemplateButtonLocator);
        scrollIntoView(courseTemplateButton, true).click();
    }

    public String getSelectedTemplateText(){
        ExtentReport.log("Trying to get the text content of the course template button");
        for(WebElement radioButtonInput: frameworkRadioButtonInputs){
            if(isRadioButtonInputChecked(radioButtonInput)){
                WebElement spanTemplateName = radioButtonInput.findElement(By.xpath(FRAMEWORK_TEMPLATE_NAME_FROM_RADIO_BUTTON_INPUT_XPATH));
                return spanTemplateName.getText();
            }
        }
        return "";
    }

    public boolean noFrameWorkIsSelected(){
        ExtentReport.log("Trying to check if any framework is selected");
        for(WebElement radioButtonInput: frameworkRadioButtonInputs){
            if(isRadioButtonInputChecked(radioButtonInput)){
                return false;
            }
        }
        return true;
    }

    public void clickMenuButtonOverCourse(String courseName) {
        ExtentReport.log(String.format("Trying to click the options menu over the course '%s'", courseName));
        String index = getUnsavedCourse(courseName).getAttribute("aria-rowindex");
        WebElement courseMenuButton = waitElementToBePresent(By.cssSelector(String.format(TABLE_UNSAVED_COURSE_MENU_BUTTON_CSS, index)));
        courseMenuButton.click();
    }

    public void clickMenuButtonOverFirstDisplayedCourse(String childrenFolderName, String parentFolderName) {
        ExtentReport.log("Trying to click the options menu button over the first displayed course");
        WebElement courseMenuButton = getFirstDisplayedSavedCourse(childrenFolderName, parentFolderName).findElement(By.xpath(COURSE_MENU_BUTTON_XPATH));
        courseMenuButton.click();
    }

    public void clickGoToCourseLinkOverFirstDisplayedCourse() {
        ExtentReport.log("Trying to click the 'Go To Course' link over the first displayed course");
        List<WebElement> courses = null;
        try{
            courses = waitNumberOfElementsToBeMoreThan(By.cssSelector(TABLE_COURSES_CSS), 0);
        } catch (TimeoutException ex){
            Assert.fail("Could not complete the test. The selected account does not contain courses");
        }
        if(courses != null){
            courses.get(0).findElement(By.cssSelector(TABLE_COURSES_LINK_CSS)).click();
        }
    }

    public void clickAdd() {
        clickWithJS(buttonAdd);
    }

    public void clickApply()  {
        ExtentReport.log("Trying to click the Apply button");
        waitElementToBeClickeable(buttonApply).click();

    }

    public void clickSyncAllChangesButton(){
        ExtentReport.log("Trying to click the Sync All Changes button");
        waitElementToBeClickeable(buttonSyncAllChanges).click();
    }

    public boolean isSyncAllChangesButtonDisplayed() {
        return isElementDisplayedCatchNotFoundException(buttonSyncAllChanges, "Sync All Changes button");
    }

    public boolean isCheckSyncStatusButtonDisplayed(){
        ExtentReport.log("Verifying if the 'Check Sync Status' button is displayed");
        try{
            return waitElementToBePresent(buttonCheckSyncStatus).isDisplayed();
        } catch (NoSuchElementException ex){
            ExtentReport.log(Arrays.toString(ex.getStackTrace()));
            return false;
        }
    }

    public void clickCloseSyncStatusWindow(){
        waitElementToBeClickeable(buttonCloseSyncStatusWindow).click();
    }

    public void clickDeleteAccount() {
        waitElementToBeClickeable(buttonDeleteAccount).click();
    }

    public void clickBulkActionsDeleteButton() {
        waitElementToBeClickeable(buttonBulkActionsDelete).click();
    }

    public boolean isApplyFrameworkButtonDisplayed(){
        return isElementDisplayedCatchNotFoundException(buttonBulkActionsApplyFramework, "Apply framework button");
    }

    public boolean isCourseSettingsButtonDisplayed(){
        return isElementDisplayedCatchNotFoundException(buttonCourseSettings, "Course Settings button");
    }

    public boolean isClearSelectionsButtonDisplayed(){
        return isElementDisplayedCatchNotFoundException(buttonClearSelections, "Clear Selections button");
    }

    public void clickGoToSubAccountFolderOption() {
        waitElementToBeClickeable(buttonGoToSubAccount).click();
    }

    public void clickListViewButton() {
        buttonListView.click();
    }

    public void clickTreeViewButton() {
        buttonTreeView.click();
    }

    public boolean isCourseDisplayed(String courseName) {
        ExtentReport.log(String.format("Trying to find the course with name '%s'", courseName));
        WebElement course;
        try{
            course = getCourse(courseName);
        } catch(TimeoutException e2){
            ExtentReport.log(String.format("Could not find the course with name '%s'", courseName));
            return false;
        }
        return course.isDisplayed();
    }

    public boolean areAllCoursesSelected() {
        List<WebElement> checkboxSelectCourseInputs = waitNumberOfElementsToBeMoreThan(By.cssSelector(TABLE_COURSE_CHECKBOX_CSS), 0);
        for(WebElement checkobox: checkboxSelectCourseInputs){
            if(!checkobox.isSelected()){
                return false;
            }
        }
        return true;
    }

    public boolean areAllCoursesUnselected() {
        List<WebElement> checkboxSelectCourseInputs = waitNumberOfElementsToBeMoreThan(By.cssSelector(TABLE_COURSE_CHECKBOX_CSS), 0);
        for(WebElement checkobox: checkboxSelectCourseInputs){
            if(checkobox.isSelected()){
                return false;
            }
        }
        return true;
    }

    public void clickCourseCheckbox(String courseName) {
        WebElement course = getCourse(courseName);
        String courseRowIndex = course.getAttribute("aria-rowindex");
        WebElement checkbox = waitElementToBePresent(By.cssSelector(String.format(TABLE_UNSAVED_COURSE_CHECKBOX_CSS, courseRowIndex)));
        checkbox.click();
    }

    public String getCell(int row, int column) {
        String text = waitNumberOfElementsToBeMoreThan(By.cssSelector(String.format(TABLE_COURSES_CELLS_CSS, column)), 0).get(row).getText();
        //Replace cell content if matches the string to ignore (given Edit text)
        if (text.contains(TABLE_COURSE_CELL_STRING_TO_IGNORE)) {
            text = text.replace(TABLE_COURSE_CELL_STRING_TO_IGNORE, "");
        }

        return text;
    }

    public List<Integer> getCourseColumnIndexes() {
        List<WebElement> inputCourseFilters = waitElementsToBePresent(new By.ByXPath(INPUT_COURSE_COLUMN_XPATH));
        //Ignore date filters
        return inputCourseFilters.subList(0, inputCourseFilters.size()-2).stream().map(
                div -> Integer.parseInt(div.getAttribute("aria-colindex"))
        ).collect(Collectors.toList());
    }

    public List<String> getCourseColumnNames() {
        List<WebElement> inputCourseFilters;
        try{
            inputCourseFilters = waitElementsToBePresent(new By.ByXPath(INPUT_COURSE_COLUMN_XPATH));
        } catch(TimeoutException e){
            return new ArrayList<>();
        }

        List<String> courseColumnNames = new ArrayList<>();
        Actions actions = new Actions(driver);
        for(WebElement column: inputCourseFilters){
            //Move to the element before getting the text
            actions.moveToElement(column).build().perform();
            courseColumnNames.add(column.getText());
        }
        return courseColumnNames;
    }

    public List<String> getCourseTags(String courseName){
        WebElement course = getCourse(courseName);
        List<WebElement> tags = course.findElements(By.xpath(TABLE_COURSE_TAG_BUTTON_SPAN_XPATH));
        //Parse tags to string (move to element is required)
        List<String> stringTags = new ArrayList<>();
        Actions actions = new Actions(driver);
        for(WebElement tag: tags){
            actions.moveToElement(tag).build().perform();
            stringTags.add(tag.getText());
        }
        return stringTags;
    }

    public String getCourseCode(String courseName) {
        WebElement course = getCourse(courseName);
        WebElement courseCode = course.findElement(By.xpath(TABLE_COURSE_CODE_CELL_XPATH));
        return courseCode.getText();
    }

    public String getCourseTermName(String courseName) {
        WebElement course = getCourse(courseName);
        WebElement courseTerm = course.findElement(By.cssSelector(TABLE_COURSE_TERM_CELL_CSS));
        return courseTerm.getText();
    }

    public void applyCourseFilter(Integer filterColumnIndex, String filterBy) {
        //Set current table row count
        int rowCount = waitElementsToBePresent(By.cssSelector(TABLE_COURSES_CSS)).size();
        //Apply filter
        ExtentReport.log("Trying to apply filter with text " + filterBy);
        List<WebElement> inputCourseFilters = waitElementsToBePresent(new By.ByXPath(INPUT_COURSE_COLUMN_XPATH));
        inputCourseFilters.forEach(divFilter -> {
            if(Integer.parseInt(divFilter.getAttribute("aria-colindex")) == filterColumnIndex){
                scrollIntoView(divFilter, true);
                divFilter.findElement(By.xpath(".//input")).sendKeys(filterBy);
            }
        });
        //Wait for the table row count to change
        waitForNumberOfRowsToChange(rowCount);
    }

    private void waitForNumberOfRowsToChange(int initialRowCount){
        Wait wait = new WebDriverWait(driver, pause);
        try{
            wait.until((ExpectedCondition<Boolean>) driver -> Integer.parseInt(divGridPanel.getAttribute("aria-rowcount")) != initialRowCount);
        } catch (TimeoutException ex){
            //Ignore if time limit is reached
        }
    }

    public void clearCourseFilter(Integer filterColumnIndex) {
        //Set current table row count
        int rowCount = waitElementsToBePresent(By.cssSelector(TABLE_COURSES_CSS)).size();
        List<WebElement> inputCourseFilters = waitElementsToBePresent(new By.ByXPath(INPUT_COURSE_COLUMN_XPATH));
        inputCourseFilters.forEach(divFilter -> {
            if(Integer.parseInt(divFilter.getAttribute("aria-colindex")) == filterColumnIndex){
                WebElement filterInput = divFilter.findElement(By.xpath(".//input"));
                //Send BACK_SPACE key instead of clearing the element with the "clear()", function given that it does not trigger the table filter update
                while(filterInput.getAttribute("value").length() > 0){
                    filterInput.sendKeys(Keys.BACK_SPACE);
                }
            }
        });
        waitForNumberOfRowsToChange(rowCount);
    }

    public List<String> getAllCellsUnderColumn(Integer column){
        return waitNumberOfElementsToBeMoreThan(By.cssSelector(String.format(TABLE_COURSES_CELLS_CSS, column)), 0)
                .stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public void clickAcceptPopUpWindow(){
        clickAssertAlertButton();
    }

    public void clickSyncButton() {
        waitElementToBeClickeable(statusSyncLabel).click();
    }

    public void clickAcceptToast(){
        waitElementToBePresent(buttonAcceptSyncToast);
        waitElementToBeClickeable(buttonAcceptSyncToast).click();
    }

    public void clickAcceptPopupButton(){
        waitElementToBePresent(By.xpath(ACCEPT_POPUP_BUTTON_XPATH)).click();
    }

    public void clickSelectAllCourses()  {
        waitElementToBeClickeable(checkboxSelectAllCourses).click();
    }

    public void clickApplyFramework(){
        waitElementToBeClickeable(buttonApplyFramework).click();
    }

    public void clickButtonShowSyncJobFirstRow() throws InterruptedException {
        Thread.sleep(5000);
        waitElementToBeClickeable(buttonShowSyncJobFirstRow).click();
    }

    public String getFirstPreviousSyncJobFolderName() {
        String nameFolder = waitElementTextLengthToBeMoreThan(folderSyncCheckFirstRow, 0 ).getText();
        return nameFolder;
    }

    public void clickSelectCourse() {
        waitElementToBeClickeable(buttonSelectFirstFolder).click();
    }

    public void clickFirstCourseRadioButtons() {
        waitElementToBeClickeable(firstCourseRadioButtons).click();
    }

    public void clickResizeAccount() {
        waitElementToBeClickeable(resizeAccountsSectionButton).click();
    }

    public boolean accountSectionIsHidden() {
        return isElementDisplayedCatchNotFoundException(accountSectionClosed, "account section is closed");
    }

    public void clickCourseNameButton(String courseName) {
       waitElementToBePresent(By.xpath(String.format(TABLE_COURSE_NAME_BUTTON_XPATH, courseName))).click();
   }

   public void clickCourseCodeButton(String courseName){
        WebElement course = getUnsavedCourse(courseName);
        WebElement courseCodeButton = course.findElement(By.xpath(TABLE_COURSE_CODE_BUTTON_XPATH));
        courseCodeButton.click();
   }

   public void clickCourseTagButton(String courseName){
       WebElement course = getUnsavedCourse(courseName);
       WebElement courseTagButton = course.findElement(By.xpath(TABLE_COURSE_TAG_BUTTON_XPATH));
       courseTagButton.click();
   }

    public void clickCourseTermButton(String courseName) {
        WebElement course = getCourse(courseName);
        WebElement courseTermButton = course.findElement(By.cssSelector(TABLE_COURSE_TERM_BUTTON_CSS));
        courseTermButton.click();
    }

    public void clickColumnsToDisplayDropdown() {
        waitElementToBeClickeable(buttonColumnsToDisplayDropdown).click();
    }

    public void checkColumnToDisplay(String columnName) {
        WebElement columnToDisplayDropdownButton = waitElementToBeClickeable(By.xpath(String.format(COLUMNS_TO_DISPLAY_DROPDOWN_BUTTON, columnName)));
        columnToDisplayDropdownButton.click();
    }

    public void checkAllColumnsToDisplay(){
        //If "Show All" button is not checked, some options are unchecked
        WebElement showAllButton = columnsToDisplayDropdownButtons.get(0);
        if(showAllButton.getAttribute("aria-checked").equalsIgnoreCase("false")){
            showAllButton.click();
        }
    }

    public void uncheckAllColumnsToDisplay(){
        //Click "Show All" button if not checked
        WebElement showAllButton = columnsToDisplayDropdownButtons.get(0);
        if(showAllButton.getAttribute("aria-checked").equalsIgnoreCase("false")){
            showAllButton.click();
        }
        //Skip the "Show All" button, and press all the others
        columnsToDisplayDropdownButtons.stream().skip(1).forEach(WebElement::click);
    }

    public List<String> getColumnsToDisplayDropdownNames(){
        //Skip the "Show all" button, which corresponds to the first button
        return columnsToDisplayDropdownButtons.stream().skip(1).map(WebElement::getText).collect(Collectors.toList());
    }

    public boolean isColumnDisplayed(String columnName) {
        List<String> courseFilterNames = getCourseColumnNames();
        return courseFilterNames.contains(columnName);
    }

    public boolean isColumnsToDisplayDropdownOpened() {
        return isElementDisplayedCatchNotFoundException(columnsToDisplayDropdownContainer, "Columns to Display dropdown container");
    }

    public void clickDuplicateCoursesButton(){
        waitElementToBeClickeable(buttonDuplicateCourses).click();
    }

    public void clickImportCourseButton(){
        waitElementToBeClickeable(buttonImportCourse).click();
    }

    public boolean isSyncingIconDisplayedForCourse(String courseName) {
        WebElement course = getSyncingCourse(courseName);
        String courseRowIndex = course.getAttribute("aria-rowindex");
        By spinningIconLocator = By.cssSelector(String.format(TABLE_COURSES_SYNCING_ICON_CONTAINER_CSS, courseRowIndex));
        return isElementDisplayedCatchNotFoundException(spinningIconLocator, "Spinning Sync icon");
    }

    public void clickEditNewTermButton() {
        waitElementToBePresent(editNewTermButton).click();
    }

    public void clickGoToCourse(String courseNamee) {
        WebElement course = getCourse(courseNamee);
        WebElement goToCourseLink = course.findElement(By.cssSelector(TABLE_COURSES_LINK_CSS));
        goToCourseLink.click();
    }

    public void clickPopupUpdateButton() {
        waitElementToBeClickeable(popoverSubmitButton).click();
    }
}


