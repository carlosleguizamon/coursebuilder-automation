package constants;

public class TestModes {

    public final static String LOCAL = "local";
    public final static String LOCAL_DEBUG = "localDebug";
    public final static String CROSSBROWSER = "crossbrowser";
    public final static String CONTAINER = "container";

}
