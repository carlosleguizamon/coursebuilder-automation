#!/bin/bash

hs_mins=$(date +'%H:%M')

echo "Running tests:"
echo "Environment: $environment"
echo "As User: $user"
echo "Should publish: $publish"

echo "using keys: " 
echo "Key for crossbroswer username: $CROSSBROWSER_USERNAME"
echo "Key for crossbrowser authkey: $CROSSBROWSER_AUTHKEY"
echo "Key for browser: $BROWSER"
echo "Key for everspring coursebuilder email: $EVERSPRING_COURSEBUILDER_EMAIL"
echo "Key for everspring coursebuilder password: $EVERSPRING_COURSEBUILDER_PASSWORD"

#This function returns TRUE or FALSE depending if its around a certain time of the day by a margin of 10 minutes
itsaround() {
    scheduletime=$1

    windowinminutes=10

    maxtime=$(date -d "$scheduletime today + $windowinminutes minutes" +'%H:%M')

    currenttime=$(date +'%H:%M')

    if [[ "$currenttime" > "$scheduletime"]] && [["$currenttime" < "$maxtime" ]]; then
        true
    else
        false
    fi


echo "Run.sh script started at $hs_mins"

elif itsaround "06:00"; then
	scripts/courseBuilder.sh "$environment" "$xrayTicketAssignee"
else
	echo "No tests to execute at $hs_mins"
fi

if [ $publish == true ]
then
    echo "Executing Xray Publisher command"
    mvn exec:exec
fi