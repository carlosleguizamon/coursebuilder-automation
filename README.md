# Test automation framework

### Run a subset of Features or Scenarios

You can specify files to run by *path*:

    -Dcucumber.options="src/test/resources/features/Feature.feature"

You can also specify what to run by *tag*:

    -Dcucumber.filter.tags="@TEST_WD-123"